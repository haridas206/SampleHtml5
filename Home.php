<?php   
session_start();
include("db\configdb.php");      
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	$d = array();
	
	while ($key = current($_POST)) {    
		$str = preg_replace('/_/', ' ', key($_POST));
		#echo $str;
		$question= explode("||",$str);
		//echo $question[0].'<br/>';
		$subittePollObj= array('question'=>$question[0],'type'=>$question[1],'answer'=>current($_POST),'surveyName'=>$_SESSION['survey_name']);
		array_push($d, $subittePollObj);	
		next($_POST);
	}	
	$surveyJson=json_encode($d);	
	try{
		$statement = $db->prepare("INSERT INTO tbl_surveysubmission(submitted_survey, submitted_date, user_id,survey_id)
		                          VALUES(:survey, :sdate, :uid, :sid)");
		$statement->execute(array(
		                          "survey" => $surveyJson,
		                          "sdate" => date('Y-m-d'),
		                          "uid" => $_SESSION['user_id'],
		                          "sid"=>$_SESSION['survey_id']
		                          ));
		$_SESSION['surveysubmitSuccess']='success';
	}
	catch( PDOException $e ){
		print_r( $e );
	}

	try {
		$updateQry = $db->prepare("UPDATE dir_users  set poll = :poll WHERE user_id=:uid");
		$updateQry->execute(array(
		                          "poll"=>'1',
		                          "uid" => $_SESSION['user_id']
		                          ));
		
	} catch (PDOException $e) {
		print_r( $e );
	}
	
	header('Location: staffpolling.php');
}
else
{
	if (isset($_REQUEST['login']))
	{
		$myusername = $_GET['login'];
		$mydep = $_GET['dep'];
		$sql = "SELECT dir_users.user_id,dir_users.user_name,dir_users.user_type,dir_users.permissions FROM dir_users  WHERE dir_users.user_code = '$myusername'";
		  //$result = mysql_query($db,$sql);
		 // $row = mysql_fetch_array($result,MYSQLI_ASSOC);
		 // $count = mysqli_num_rows($result)
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$row = $stmt->fetchAll();
		if(count($row)==1)
		{
			$_SESSION['login_user'] = $row[0]['user_name'];
			$_SESSION['user_id'] = $row[0]['user_id'];
			$_SESSION['user_dep'] = $mydep;
			$_SESSION['userType'] =$row[0]['user_type'];
			$_SESSION['totalSuveyCount']='';
			$permision=json_decode($row[0]['permissions']);
			print_r($permision->permission);
			if (in_array("1", $permision->permission)) {
				$_SESSION['view_Dashborad']=1;
			}
			else
			{
				$_SESSION['view_Dashborad']=0;
			}
			if (in_array("2", $permision->permission)) {
				$_SESSION['view_Setting']=1;
			}
			else
			{
				$_SESSION['view_Setting']=0;
			}
			print $_SESSION['view_Dashborad'];
			if($_SESSION['view_Dashborad']==1 || $_SESSION['view_Setting']==1)
			{
				if($_SESSION['view_Dashborad']==1)
				{
					header('Location: dashboard.php'); 
				}
				else 
				{
					header('Location: SettingPanel.php'); 
				}
				
			}
			else
			{
				header('Location: staffpolling.php'); 
			}
			
		}			 
		else {
			header('Location: ./includes/error.php');
		}


	}
	else if (isset($_REQUEST['logout']))
	{
		$_SESSION['login_user'] = '';		  
		$_SESSION['user_dep'] = '';
		$_SESSION['userType'] ='';                
		$_SESSION['PollData']='';
		$_SESSION['user_id']='';
		$_SESSION['survey_id']='';
		$_SESSION['CommonPollData']='';
		$_SESSION['common_survey_id']='';
		$_SESSION['category']='';
		$_SESSION['survey_items']='';
		$_SESSION['surveysubmitSuccess']='';
		$_SESSION['totalSuveyCount']='';
		header('Location: logout.php');	 
	}

}

?>
