<!DOCTYPE html>
<script>
 function switchVisible() {
  document.getElementById('Div1').style.display = 'block';
}

</script>
<head>
  <meta charset="utf-8">
  <title>EMPLOYEE PORTAL| New Survey</title>
  <script src="js/html5-trunk.js"></script>
  <link href="icomoon/style.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">  
</head>
<body>
  <?php 
  


  include('./includes/header.php');
  try{
    $catgorySql="SELECT id,Category_Name FROM category WHERE status='1'";
    $category=$db->prepare($catgorySql);
    $category->execute();
    $row_category=$category->fetchAll();
    $_SESSION['category']=$row_category;
  }     
  catch( PDOException $e ){
    print_r( $e );
  }
  try{
    $surveySql="SELECT id,SurveyName FROM questions_survey WHERE status='1'";
    $survey=$db->prepare($surveySql);
    $survey->execute();
    $row_survey=$survey->fetchAll();
    $_SESSION['survey_items']=$row_survey;
  }     
  catch( PDOException $e ){
    print_r( $e );
  }

  if(isset($_SESSION['surveySuccess']) && $_SESSION['surveySuccess']==='success')
    echo "<script type='text/javascript'> $(window).load(function(){ $('#successModal').modal('show'); }); </script>";
  if(isset($_SESSION['surveyfailed']) && $_SESSION['surveyfailed']==='Failed')
    echo "<script type='text/javascript'> $(window).load(function(){ $('#failedModal').modal('show'); }); </script>";


  if ($_SESSION['view_Setting'] =="0"){

    echo "<style>
#DivFiltter {
    display: none;
  }
#DivSett {
  display: none;
}
</style>";
}
else
{
  echo "<style>
  #DivNoPermission {
  display: none;
}
</style>";
}

$_SESSION['surveySuccess']='';
$_SESSION['surveyfailed']=''

?>
<style>
  .form-group {      

  }
  .row {
    margin-right: 0;
    margin-left: 0;
  }

  #fieldsetDiv {
    padding: 0;
    margin: 0;
    border: 0;
    padding-left: 10px;
    vertical-align: bottom;
  }
  #DivFetch{
    display: inline-block;
    margin-bottom: 0;
    padding-right: 0;
    padding-left: 20px;
  }   
  .modal {
    text-align: center;
  }

  @media screen and (min-width: 768px) { 
    .modal:before {
      display: inline-block;
      vertical-align: middle;
      content: " ";
      height: 100%;
    }
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }
</style>


<div class="container-fluid">

    <!-- Navigation ----------------------------------------------------------------Navigation-----------------------Navigation --------->
    <div id="mainnav" class="hidden-phone hidden-tablet">
      <ul>
        <li >         
          <a href="dashboard.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe0a1;"></span>
            </div>
            Dashboard
          </a>
        </li>
        <li>
          <a href="my profile.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe071;"></span>
            </div>
            My Profile
          </a>
        </li>
        <li>
          <a href="Whatsup.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe1b3;"></span>
            </div>
            WHATSAPP
          </a>
        </li>
        <li>
          <a href="staffpolling.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            STAFF POLLING
          </a>
        </li>
        <br/>
        <li class="active">
         <span class="current-arrow"></span>      
        <a href="SettingPanel.php">
            <div class="fs1">
              <span class="icon-cog-2" aria-hidden="true" data-icon="&#xe994"></span>
            </div>
            SETTINGS
          </a>
        </li>
      </ul>
    </div>	
	
    <div class="dashboard-wrapper">
      <div class="main-container">
        <!----------------------------------------------------navigationMini------------------------------------------------------------------------- -->
        <div class="navbar hidden-desktop">
          <div class="navbar-inner">
            <div class="container">
              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
                  <li>
                    <a href="dashboard.php">Dashboard</a>
                  </li>
                  <li>
                    <a href="my profile.php">My Profile</a>
                  </li>
                  <li>
                    <a href="whatsup.php">What's Up</a>
                  </li>
                  <li>
                    <a href="#">Staff Polling</a>
                  </li>
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row-fluid"  id="DivFiltter">
          <div class="span12">
            <ul class="breadcrumb-beauty">
              <li>
                <!-------------------- EDIT HERE DASHBOARD ---------------------->
                <h4 class='text-primary'><span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span> Settings  </h4>              
              </li>
              <li>
              </li>
            </ul>
            <div class="row-fluid ">
              <div class="span12">
                <div class="widget no-margin">
                  <div class="widget-header">
                    <div class="title">
                      <span class="fs1" aria-hidden="true" data-icon=""></span> Add Survey   
                    </div>
                  </div>                     
                </div>
                <div class="tools pull-right">
                </div>          
              </div>
              <div class="widget-body">
                <div id="content">   
                  <div class="container">
                    <div cls ="row form-inline">
                     <form method="post" action='data.php'>
                       <div class="col-sm-12 panel-heading" >  
                        <div class='col-sm-3'>                      
                          <div class="form-group"> 
                            <label for="sel3">Survey Name</label> 
                            <input type="text" required="true" id='newSurvey' name="newSurvey" placeholder='Survey Name'>
                          </div>                       
                        </div>
                        <div class='col-sm-3'>                      
                          <div class="form-group"> 
                            <label for="sel3">Survey For</label> 
                            <select class="form-control input-sm" name="survey" >
                              <?php foreach($_SESSION['category'] as $category)
                              {?>
                              <option value="<?php print  $category['Category_Name']; ?>"><?php print  $category['Category_Name'] ; ?></option>
                              <?php } 
                              ?>                         
                            </select>
                          </div>                       
                        </div>
                        <div class='col-sm-3'>  
                         <label class="control-label" for="date">Valid From</label>
                         <input class="form-control" required="true" name='surveyStartdate' id='surveyStartdate'    placeholder="DD-MM-YYYY" type="text"/>
                       </div>
                       <div class='col-sm-3'> 
                         <label class="control-label" for="date">Valid To</label>
                         <input class="form-control" required="true"   name='surveyEnddate' id='surveyEnddate'  placeholder="DD-MM-YYY" type="text"/>
                         <button class="btn btn-primary " name="addSurevyBtn" id="addSurevyBtn" type="submit">Add</button>
                       </div>	
                     </div>
                   </form>

                 </div>				    
               </div>  
             </div>
           </div>
         </div>
         <div class="modal fade" id="successModal" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Success</h4>
              </div>
              <div class="modal-body">
                <div class="alert alert-success" role="alert">
                  <strong>Well done!</strong> You successfully updated Survey.
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="failedModal" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Warning</h4>
              </div>
              <div class="modal-body">
                <div class="alert alert-warning">
                  <strong>Warning!</strong> Something went wrong.Please check Date range or Survey name and try agian.
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
              </div>
            </div>
          </div>
        </div>


        <br/>
        <div class="row-fluid ">
          <div class="span12">
            <div class="widget no-margin">
              <div class="widget-header">
                <div class="title">
                  <span class="fs1" aria-hidden="true" data-icon=""></span> View/Edit Survey   
                </div>
              </div>                     
            </div>
            <div class="tools pull-right">
            </div>          
          </div>
          <div class="widget-body">
            <div id="content">   
              <div class="container">
                <div cls ="row form-inline">
                  <div class='col-sm-4'>                      
                    <div class="form-group"> <!-- Date input -->
                      <label for="sel3">Survey For</label> 
                      <select class="form-control input-sm" name="survey" id="surveyDropdown">
                        <?php foreach($_SESSION['survey_items'] as $survey)
                        {?>
                        <option value="<?php print  $survey['SurveyName']; ?>"><?php print  $survey['SurveyName'] ; ?></option>
                        <?php } 
                        ?>                             
                      </select>
                    </div>                       
                  </div>
                  <div class='col-sm-4'>  
                   <label class="control-label" for="date">Valid From</label>
                   <input class="form-control" id="startdate" name="startdate" required="true"     placeholder="DD-MM-YYYY" type="text"/>
                 </div>
                 <div class='col-sm-4'> 
                   <label class="control-label" for="date">Valid To</label>
                   <input class="form-control" id="enddate"  name="enddate" required="true"   placeholder="DD-MM-YYY" type="text"/>
                 </div>
               </div>				  
             </div>  

           </div>
         </div>
       </div>
     </div>
   </div>
   <div class="row-fluid"  id="DivNoPermission">
    <div class="span12">
      <div>
        <div class="widget no-margin">
          <div class="widget-header">
            <div class="icontype">

            </div>
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Warning 
            </div>
          </div>
          <div class="widget-body">
            <p class="text-primary"> No Permission to access this page</p>
          </div>
        </div> 
      </div>
    </div>
  </div>  

  <div class="panel panel-default" id="DivSett">  
   <div class="panel-heading">Survey elements</div>   
   <div class="panel-body">     
     <form method="post" action="surveyData.php" class="form-horizontal" id="SurveyForm"> 
       <div id='formDiv'>
         <fieldset>
           <div class="form-group">
            <div class="col-sm-12 panel-heading" >            
              <div class='col-sm-3'>  
                <input type="text"  class="form-control" name="edittedSurvey"  id='legTitle' required="true">                  
              </div>
              <div class='col-sm-3'>        
                <input id="upstartdate"  class="form-control"  name="editedstartdate" required="true"     placeholder="DD-MM-YYYY" type="text"/>
              </div>
              <div class='col-sm-3'>                  
                <input  id="upenddate"  class="form-control"   name="editedenddate" required="true"   placeholder="DD-MM-YYYY" type="text"/>
              </div>
              <div class='col-sm-3'>
               <button class="btn btn-primary " name="update" id="updateBtn" type="button">Update</button>
               <button class="btn btn-primary " name="remove" id="removeBtn" type="button">Remove</button>
             </div>
           </div> 
         </div>
       </fieldset>                
       <fieldset>
         <div class="form-group">
          <label class="col-sm-2 control-label">Question</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id='question' required="true">
            <span class="help-block m-b-none">A block of help text that breaks onto a new line and may extend beyond one line.</span>
          </div>
        </div>
      </fieldset>
      <fieldset>
        <div class="form-group">
          <label class="col-sm-2 control-label">Answer Type</label>
          <div class="col-sm-8">                
            <select class="form-control input-sm"  name="optionType" id="optionType">
              <option value="single-select">Single Select</option>
              <option value="Custom-Text">Custom-Text</option>                          
            </select> 
            <div id='singleDiv' >
              <span class="help-block m-b-none"><input  type="radio" name="opt"  id="yesno">Yes/No</span>
              <span class="help-block m-b-none"> <input type="radio" name="opt"  id="good">Excellent/Very Good/Good/Average/Poor</span><br>
            </div>
          </div>               

        </div>
      </fieldset>
      <fieldset>
        <div class="form-group">
          <label class="col-sm-2 control-label">Required</label>
          <div class="col-sm-8"> 
            <span class="help-block m-b-none"><input type="radio" name="requiredOpt"  id="requiredOptYes">Yes</span>
            <span class="help-block m-b-none"> <input type="radio" name="requiredOpt" id="requiredOptNo">No</span><br>                
          </div>          

        </div>
      </fieldset>
    </div>    
    <div id='DivFetch' class="form-group"> 
     <button class="btn btn-primary " name="prev" id="prevBtn" type="button">Previous</button>   
     <button class="btn btn-primary " name="next" id="nxtBtn" type="button">Next</button>             
     <button class="btn btn-primary " name="submit" type="button" id="saveBtn">Save</button>
     <button class="btn btn-primary " name="edit" id="editBtn" type="button">Edit</button>
     <button class="btn btn-primary " name="cancel" id="cancelBtn" type="button">Cancel</button>
     <button class="btn btn-primary " name="newQues" id="addBtn" type="button">New</button>
   </div> 
 </form>
</div>
</div>       
</div>     
</div><!-- dashboard-container -->

</div>

</div>
<!-- container-fluid -->
<?php include('./includes/footer.php')?>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>

<!-- Calendar Js -->
<script src="js/fullcalendar.js"></script>
<!-- Tiny Scrollbar JS -->
<script src="js/tiny-scrollbar.js"></script>   

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script> 
  var curentSurevyIndex=0;
  var SurveyCollection=[];
  var EdittedSurveyCollection={};
  var EdittedSurveyobj=[];
  var surveyIndex=0;
  var isNewQuestion=0;
  var surveyId=0;
  var currentSurveyName='';
  var surveyStartDate='';
  var surveyEndDate='';
  $(document).ready(function(){

    $("#startdate").attr('disabled', true);
    $("#enddate").attr('disabled', true);

		//$("#thankyouModal").modal('show');
        var startdate_input=$('input[name="surveyStartdate"]'); //our date input has the name "date"
        var enddate_input=$('input[name="surveyEnddate"]'); 
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        startdate_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
          selected:true,
        })
        enddate_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
        })
        var upstartdate_input=$('input[name="editedstartdate"]'); //our date input has the name "date"
        var upenddate_input=$('input[name="editedenddate"]');         
        upstartdate_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
          selected:true,
        })
        upenddate_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
        })

        $.ajax({
          type: "GET",
          url: "data.php",
          dataType: "json",
          success: function(data) {

            sessionStorage.setItem('surveyData', JSON.stringify(data)); 
            setSurveyData();
            $("#prevBtn").attr('disabled', true);          
            enableView()
          }
        })
        function setNewQuestion(){

          var question=document.getElementById('question');          
          question.value='';          
          $("#nxtBtn").attr('disabled', true);
        }
        function setNewSurvey(){
          var startdate=document.getElementById('startdate');
          var enddate=document.getElementById('enddate');
          var question=document.getElementById('question');
          startdate.value= '';
          enddate.value= ''
          question.value=''; 
          $("#prevBtn").attr('disabled', true);
          $("#nxtBtn").attr('disabled', true);

          
        }
        function enableView(){
          $("#addBtn").hide();
          $("#saveBtn").hide();
          $("#cancelBtn").hide();
          $("#prevBtn").show();
          $("#nxtBtn").show();
          $("#editBtn").show();          
          $("#question").attr('disabled', true);
          $("#optionType").attr('disabled', true);
          $("#yesno").attr('disabled', true);
          $("#good").attr('disabled', true);
          $("#requiredOptYes").attr('disabled', true);
          $("#requiredOptNo").attr('disabled', true);
          
        }
        function enableEdit(){
         $("#addBtn").show();
         $("#saveBtn").show();
         $("#cancelBtn").show();
         $("#prevBtn").hide();
         $("#nxtBtn").hide();
         $("#editBtn").hide();
         $("#question").removeAttr('disabled');
         $("#optionType").removeAttr('disabled');
         $("#yesno").removeAttr('disabled');
         $("#good").removeAttr('disabled');
         $("#requiredOptYes").removeAttr('disabled');
         $("#requiredOptNo").removeAttr('disabled');
       }
       $("#editBtn").click(function(){
        enableEdit();
      })
       $("#cancelBtn").click(function(){
         enableView();
         if(isNewQuestion==1)
         {
           setSurveyData()
         }


       })
       function setSurveyData(){
        if(typeof(Storage) !== "undefined") {        

         var surveyData = JSON.parse(sessionStorage.getItem('surveyData')); 
         surveyId=surveyData[surveyIndex].id;
         currentSurveyName=surveyData[surveyIndex].SurveyName;

         SurveyCollection=JSON.parse(surveyData[surveyIndex].survey);           
         document.getElementById('legTitle').value=surveyData[surveyIndex].SurveyName         
         var startdate=document.getElementById('startdate');
         var enddate=document.getElementById('enddate');
         var question=document.getElementById('question');
         var dateAr = surveyData[surveyIndex].start_date.split('-');
         var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0];
         startdate.value= newDate;
         surveyStartdate=newDate;
         upstartdate.value=newDate;

         var enddateAr = surveyData[surveyIndex].end_date.split('-');
         var newendDate = enddateAr[2] + '-' + enddateAr[1] + '-' + enddateAr[0];
         enddate.value= newendDate;
         upenddate.value=newendDate
         surveyEndDate=newendDate;
         question.value=SurveyCollection.Survey[curentSurevyIndex].question   ;           
         $("#optionType").val(SurveyCollection.Survey[curentSurevyIndex].type);
         if(SurveyCollection.Survey.length>1)
         {
          $("#nxtBtn").removeAttr('disabled');
        }         
        else
        {
          $("#prevBtn").attr('disabled', true);
          $("#nxtBtn").attr('disabled', true);
        }
        if(SurveyCollection.Survey[curentSurevyIndex].type=='single-select')
        {
          $("#yesno").removeAttr('disabled');
          $("#good").removeAttr('disabled');
          if(SurveyCollection.Survey[curentSurevyIndex].options.length<3)
          {
            document.getElementById('yesno').checked=true;
          }
          else
          {  

            document.getElementById('good').checked=true;    

          }
        }
        else
        {
          $("#yesno").attr('disabled', true);
          $("#good").attr('disabled', true);
        }
        if(SurveyCollection.Survey[curentSurevyIndex].required==true)
        {
          document.getElementById('requiredOptYes').checked=true;
        }
        else
        {
          document.getElementById('requiredOptNo').checked=true;
        }
      }        
    }
    $("#optionType").change(function(){
      $(this).find("option:selected").each(function(){
        var optionValue = $(this).attr("value");
        if(optionValue=='single-select'){
          $("#yesno").removeAttr('disabled');
          $("#good").removeAttr('disabled');
        } else{
          $("#yesno").attr('disabled', true);
          $("#good").attr('disabled', true);
        }
      });
    }).change();

    $("#nxtBtn").click(function(){
      debugger;
      if (document.getElementById('question').value=='') {            
        document.getElementById('question').focus();
        return  ;
      }

      $("#prevBtn").removeAttr('disabled');
      if(SurveyCollection.Survey.length>curentSurevyIndex)
      {
        curentSurevyIndex=curentSurevyIndex+1;
        setSurveyData();
      }
      if(SurveyCollection.Survey.length-1==curentSurevyIndex)
      {
        $("#nxtBtn").attr('disabled', true);
      }


    })
    $("#updateBtn").click(function(){
      if(document.getElementById('upstartdate').value == surveyStartdate && document.getElementById('upenddate').value == surveyEnddate && document.getElementById('legTitle').value == surveyName)
      {
        return;
      }

      if (document.getElementById('legTitle').value=='') {            
        document.getElementById('legTitle').focus();
        return  ;
      }
      if (document.getElementById('upstartdate').value=='') {            
        document.getElementById('upenddate').focus();
        return  ;
      }
      if (document.getElementById('upenddate').value=='') {            
        document.getElementById('upenddate').focus();
        return  ;
      }
      var dataString = {
        surevyid:surveyId,
        surveyName:currentSurveyName,
        editedstartdate: document.getElementById('upstartdate').value,
        edittedenddate: document.getElementById('upenddate').value,
        edittedsurvey: document.getElementById('legTitle').value,
        update:'update'
      }
      $.ajax({
        type: "POST",
        url: "data.php",
        dataType: "json",
        data: dataString,
        success: function(data) {
          sessionStorage.setItem('surveyData', JSON.stringify(data)); 
          location.reload();
        }
      })
    })
    $("#removeBtn").click(function(){
      if (document.getElementById('legTitle').value=='') {            
        document.getElementById('legTitle').focus();
        return  ;
      }
      if (document.getElementById('upstartdate').value=='') {            
        document.getElementById('upstartdate').focus();
        return  ;
      }
      if (document.getElementById('upenddate').value=='') {            
        document.getElementById('upenddate').focus();
        return  ;
      }
      document.getElementById('upstartdate').value=document.getElementById('startdate').value
      document.getElementById('upenddate').value=document.getElementById('enddate').value
      document.getElementById('legTitle').value=currentSurveyName
      var dataString = {
        surevyid:surveyId,
        remove:'remove'
      }
      $.ajax({
        type: "POST",
        url: "data.php",
        dataType: "json",
        data: dataString,
        success: function(data) {
          sessionStorage.setItem('surveyData', JSON.stringify(data)); 
          location.reload();
        }
      })
    })
    
    $("#prevBtn").click(function(){
     if (document.getElementById('question').value=='') {            
      document.getElementById('question').focus();
      return  ;
    }
    $("#nxtBtn").removeAttr('disabled');
    curentSurevyIndex=curentSurevyIndex-1;
    setSurveyData();          
    if(curentSurevyIndex==0)
    {
      $("#prevBtn").attr('disabled', true);
    }

  })
    $("#addBtn").click(function(){
      setNewQuestion();
      isNewQuestion=1;

    })
    $("#saveBtn").click(function(){  
      debugger;
      if (document.getElementById('question').value=='') {            
        document.getElementById('question').focus();
        return  ;
      }
      if(isNewQuestion==0)
      {
        SurveyCollection.Survey[curentSurevyIndex].question=document.getElementById('question').value;
        SurveyCollection.Survey[curentSurevyIndex].type=document.getElementById('optionType').value
        if(SurveyCollection.Survey[curentSurevyIndex].type=='Custom-Text')
        {
         SurveyCollection.Survey[curentSurevyIndex].options=[];
       }
       else
       {
         if(document.getElementsByName('opt')[0].checked == true)
         {
          SurveyCollection.Survey[curentSurevyIndex].options=["Yes","No"];
        }
        else
        {
         SurveyCollection.Survey[curentSurevyIndex].options=["Excellent","Very Good","Good","Average","Poor"];
       }

     }
     SurveyCollection.Survey[curentSurevyIndex].required=document.getElementsByName('requiredOpt').value;
   }
   else
   {
     var obj={};
     obj.question=document.getElementById('question').value;
     obj.id=SurveyCollection.Survey.length+1;
     obj.type=document.getElementById('optionType').value
     if(obj.type == 'Custom-Text')
     {
       obj.options=[];
     }
     else
     {
      if(document.getElementsByName('opt')[0].checked == true)
      {
       obj.options=["Yes","No"];
     }
     else
     {
       obj.options=["Excellent","Very Good","Good","Average","Poor"];
     }
   }
   obj.required=document.getElementsByName('requiredOpt').value;
   obj.break_after=true;
   obj.isExtraComment='No';
   SurveyCollection.Survey.push(obj);
 }
 isNewQuestion==0;
 var dataString = {
  surveyName:document.getElementById('legTitle').value,
  surveyData:SurveyCollection
};      
  //event.preventDefault();       
var posting = $.post( 'surveyData.php', dataString );       
posting.done(function(data) {
  if(data=='Success')
  { 
    $('#successModal').modal('show'); 
    debugger;
    $.ajax({
      type: "GET",
      url: "data.php",
      dataType: "json",
      success: function(data) {
       sessionStorage.clear();       
       sessionStorage.setItem('surveyData', JSON.stringify(data)); 
       setSurveyData();
       enableView();
		   //$("#thankyouModal").modal('show');
     }
   })	

  }

});
})

    $('input').on('keypress', function (event) {
     var regex = new RegExp("^[a-zA-Z0-9?',._ ]+|[\b]+$");
     var key = String.fromCharCode(!event.charCode ? event.which : event.charCode); 
     if (!(key == 8 || key == 27 || key == 46 || key == 37 || key == 39)) {
       if (!regex.test(key)) {
         event.preventDefault();
         return false;
       }
     }
     else
      return true;
  });
    $('#surveyDropdown').change(function(){ 
      debugger;
      if(sessionStorage.getItem('surveyData') == null)
        return;
      var surveyData = JSON.parse(sessionStorage.getItem('surveyData'));
      $(this).find("option:selected").each(function(){
       var optionValue = $(this).attr("value");
       surveyIndex=-1;
       for (var i = 0; i<surveyData.length; i++){            
        if(optionValue==surveyData[i].SurveyName)
        {
          surveyIndex=i;              
        }            
      }
      if(surveyIndex!=-1)
      {
        $("#prevBtn").attr('disabled', true);
        curentSurevyIndex=0;
        enableView();
        setSurveyData(); 
      }
      else
        setNewSurvey();

    });
    }).change();
  })
</script>

</body>
</html>
