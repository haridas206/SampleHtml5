
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>EMPLOYEE PORTAL| STAFF POLLING</title>
  <script src="js/html5-trunk.js"></script>
  <link href="icomoon/style.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">
  
  <!-- new -->
  <script src="js/html5-trunk.js"></script>
  <link href="icomoon/style.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
    <!--[if lte IE 7]>
      <script src="css/icomoon-font/lte-ie7.js"></script>
      <![endif]-->
      <link href="css/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet">
      <link href="css/wysiwyg/wysiwyg-color.css" rel="stylesheet">
      <link href="css/timepicker.css" rel="stylesheet">
      <link href="css/bootstrap-editable.css" rel="stylesheet">
      <link href="css/select2.css" rel="stylesheet">
      <style>

        #el01 {width:100%} /* Width */
        #el02 { /* Text and background colour, blue on light gray */
          color:#00f;
          background-color:#ddd;
        }
        #el03 {background:url(/i/icon-info.gif) no-repeat 100% 50%} /* Background image */
        #el04 {border-width:6px} /* Border width */
        #el05 {border:2px dotted #00f} /* Border width, style and colour */
        #el06 {border:none} /* No border */
        #el07 {padding:1em} /* Increase padding */
        #el08 { /* Change width and height */
          width:4em;
          height:4em;
        }

      </style>

    </head>
    <body>
      <!-- Header -->


      <?php include('./includes/header.php');

      if(isset($_SESSION['surveysubmitSuccess']) && $_SESSION['surveysubmitSuccess']==='success')
        echo "<script type='text/javascript'> $(window).load(function(){ $('#successModal').modal('show'); }); </script>";
      try{
        $commonSurveySql= "SELECT id,survey,SurveyName FROM questions_survey a WHERE surveyDep='CM' and status='1' and '".date('Y-m-d')."' between start_date and end_date and not exists(select 1 from tbl_surveysubmission b where a.id=b.survey_id and b.user_id='".$_SESSION['user_id']."' and b.status='1')"; 
        $commonSurvey=$db->prepare($commonSurveySql);
        $commonSurvey->execute();
        $row_common_survey=$commonSurvey->fetchAll();
      }     
      catch( PDOException $e ){
        print_r( $e );
      }
      $_SESSION['surveysubmitSuccess']='';
      if(count($row_common_survey)==1)
      {
        $_SESSION['CommonPollData']=json_decode($row_common_survey[0]['survey']);
        $_SESSION['common_survey_id']=$row_common_survey[0]['id'];
        $_SESSION['common_survey_name']=$row_common_survey[0]['SurveyName'];
        #print_r($_SESSION['PollData']); exit();
        
      }
      else{
       $_SESSION['CommonPollData']='';
       $_SESSION['common_survey_id']='';
     }
     try {
      $serverSql= "SELECT id,survey,SurveyName FROM questions_survey a WHERE surveyDep='".$_SESSION['user_dep']."' and status='1' and '".date('Y-m-d')."' between start_date and end_date and not exists(select 1 from tbl_surveysubmission b where a.id=b.survey_id and b.user_id='".$_SESSION['user_id']."' and b.status='1')"; 
      $survey=$db->prepare($serverSql);
      $survey->execute();
      $row_survey=$survey->fetchAll();   
    }       
    catch( PDOException $e ){
      print_r( $e );
    }

    if(count($row_survey)==1)
    {

      $_SESSION['PollData']=json_decode($row_survey[0]['survey']);
      $_SESSION['survey_id']=$row_survey[0]['id'];
      $_SESSION['survey_name']=$row_survey[0]['SurveyName'];
     // print($_SESSION['survey_name']); exit();

    }
    else
    {
      $_SESSION['PollData']='';
      $_SESSION['survey_id']='';
      $_SESSION['survey_name']='';
    }
    if ($_SESSION['user_dep'] =="D"){

      echo "<style>
    #DivS {
      display: none;
    }
    #DivN {
    display: none;
  }  
  

</style>";
}
if ($_SESSION['user_dep'] =="N"){
  echo	"<style>
#DivS {
  display: none;
}
#DivD {
display: none;
}

</style>";

}
if ($_SESSION['user_dep'] =="S"){


  echo  "<style>
#DivD {
  display: none;
}
#DivN {
display: none;
}


</style>";
}
if(!isset($_SESSION['CommonPollData']) ||$_SESSION['CommonPollData'] == ''){
  echo  "<style>
#DivA {
  display: none;  
}

</style>";
}
if(!isset($_SESSION['PollData']) || $_SESSION['PollData'] == ''){
  echo  "<style>
#DivD {
  display: none;  
}
#DivN {
display: none;
}
#DivS {
display: none;
}

</style>";
}
if((!isset($_SESSION['PollData']) || $_SESSION['PollData'] == '') && (!isset($_SESSION['CommonPollData']) ||$_SESSION['CommonPollData'] == '')){
  echo  "<style>
#DivD {
  display: none;  
}
#DivN {
display: none;
}
#DivS {
display: none;
}
#DivA {
display: none;
}
</style>";
}
else
{
  echo  "<style>
  #DivNoData {
  display: none;
}
</style>";

}
?>
<div class="container-fluid">
  <!-- Navigation ----------------------------------------------------------------Navigation-----------------------Navigation --------->
  <div id="mainnav" class="hidden-phone hidden-tablet">
    <ul>	
      <li class="">
	  
        <a href="dashboard.php">  
        <div class="icon" id="dashDiv" >
                <span class="fs1" aria-hidden="true" data-icon="&#xe0a1;"></span>             
              Dashboard
			  </div> 
            </a>
			         </li>
		   
          <li>
            <a href="my profile.php">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe071;"></span>
              </div>
              My Profile
            </a>
          </li>
          <li>
            <a href="Whatsup.php">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe1b3;"></span>
              </div>
              WHATSAPP
            </a>
          </li>
          <li class="active">
           <span class="current-arrow"></span>
           <a href="staffpolling.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            STAFF POLLING
          </a>
        </li>
		<br/>
        <li>        
          <a href="SettingPanel.php">
            <div class="fs1" id="SetDiv">
              <span class="icon-cog-2" aria-hidden="true" data-icon="&#xe994"></span>
			  SETTINGS
            </div>
            
          </a>
        </li>
      </ul>
    </div>
    <div class="dashboard-wrapper">
      <div class="main-container">
        <!----------------------------------------------------navigationMini------------------------------------------------------------------------- -->
        <div class="navbar hidden-desktop">
          <div class="navbar-inner">
            <div class="container">
              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
                  <li>
                    <a href="index1.php">Dashboard</a>
                  </li>
                  <li>
                    <a href="my profile.php">My Profile</a>
                  </li>
                  <li>
                    <a href="whatsup.php">What's Up</a>
                  </li>
                  <li>
                    <a href="staffpolling.php">Staff Polling</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row-fluid" id="DivS">
          <div >
            <div class="span12">
              <ul class="breadcrumb-beauty">
                <li>
                  <a href="index.html"><span class="fs1" aria-hidden="true" data-icon=""></span> Staff Polling</a>
                </li>
                <li>

                </li>
              </ul>
            </div>
          </div>
        </div>       
        <div class="row-fluid" id="DivD">
          <div class="span12">
            <div >
              <div class="widget no-margin">
                <div class="widget-header">
                  <div class="icontype">

                  </div>
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span> Doctor Polling 
                  </div>
                </div>
                <div class="widget-body">
                  <?php include('./PollingForm.php'); ?>
                </div>
              </div> 
            </div>
          </div>
        </div>       
        <div class="row-fluid">
          <div class="span12">
            <div id="DivN">


              <div class="widget no-margin">
                <div class="widget-header">
                  <div class="icontype">

                  </div>
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span> Nurse Polling 
                  </div>
                </div>
                <div class="widget-body">
                 <?php include('./PollingForm.php'); ?>
               </div>
             </div> 
           </div>
         </div>
       </div>      
       <div class="row-fluid" id="DivS">
        <div class="span12">
          <div >
            <div class="widget no-margin">
              <div class="widget-header">
                <div class="icontype">

                </div>
                <div class="title">
                  <span class="fs1" aria-hidden="true" data-icon=""></span> Staff Polling 
                </div>
              </div>
              <div class="widget-body">
               <?php include('./PollingForm.php'); ?>
             </div>
           </div> 
         </div>
       </div>
     </div> 
     <div class="modal fade" id="successModal" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Success</h4>
          </div>
          <div class="modal-body">
            <div class="alert alert-success" role="alert">
              <strong>Well done!</strong> You successfully Submitted Survey.
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>
    <div class="row-fluid"  id="DivNoData">
      <div class="span12">        
        <div class="widget no-margin">
          <div class="widget-header">
            <div class="icontype">

            </div>
            <div class="title">
              <span class="fs1" aria-hidden="true" data-icon=""></span> Polling 
            </div>
          </div>
          <div class="widget-body">
            <p class="text-primary"> No Survey Available</p>
            <div class="container">
              <div class="row">
               <div class='col-sm12'>
                <div class='col-sm-6'>
                  <div id="allStat" style="min-width: 310px; height: 400px; margin: 0 auto"> </div>
                </div>
                <div class='col-sm-6'>          
                  <div id="questStat" style="min-width: 310px; height: 400px; margin: 0 auto"> </div>
                </div>
              </div>
            </div> 
          </div>
        </div>          
      </div>
    </div>     
  </div>
  <div class="row-fluid" id="DivA">
    <div class="span12">
      <div >
       <div class="widget no-margin">
        <div class="widget-header">
          <div class="icontype">

          </div>
          <div class="title">
            <span class="fs1" aria-hidden="true" data-icon=""></span> Common Polling 
          </div>
        </div>
        <div class="widget-body">
         <?php include('./CommonPollingForm.php'); ?>
       </div>
     </div> 
   </div>
 </div>
</div>





</div>
</div>
</div>
</div>

</div>

</div>
</div>

<?php include('./includes/footer.php')?>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery-ui-1.8.23.custom.min.js"></script>

<!-- morris charts -->
<script src="js/morris/morris.js"></script>
<script src="js/morris/raphael-min.js"></script>

<!-- Flot charts -->
<script src="js/flot/jquery.flot.js"></script>
<script src="js/flot/jquery.flot.resize.min.js"></script>

<!-- Calendar Js -->
<script src="js/fullcalendar.js"></script>

<!-- Tiny Scrollbar JS -->
<script src="js/tiny-scrollbar.js"></script>

<!-- custom Js 
  <script src="js/custom-index.js"></script>
  <script src="js/custom.js"></script>-->

  <!-- New -->
  <script src="js/wysiwyg/wysihtml5-0.3.0.js"></script>
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/wysiwyg/bootstrap-wysihtml5.js"></script>
  <script type="text/javascript" src="js/date-picker/date.js"></script>
  <script type="text/javascript" src="js/date-picker/daterangepicker.js"></script>
  <script type="text/javascript" src="js/bootstrap-timepicker.js"></script>

  <!-- Editable Inputs -->
  <script src="js/bootstrap-editable.min.js"></script>
  <script src="js/select2.js"></script>
  <!--<script src="js/npm.js"></script>-->

  <!-- custom Js -->
  <!--<script src="js/custom-forms.js"></script>
  <script src="js/custom.js"></script>-->
  <!-- from html -->

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/jquery.dataTables.js"></script>
  <script src="js/jquery.sparkline.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="http://code.highcharts.com/highcharts-more.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <!-- Custom Js -->
  <!--<script src="js/custom-tables.js"></script>
  <script src="js/custom.js"></script> -->
  <script>
   $(document).ready(function () {



    function hiding(){
     $( '#DivS').hide();
   }
   function hiding(){
     $( '#DivD').hide();
   }
   function hiding(){
     $( '#DivN').hide();
   }
   function hiding(){
     $( '#DivA').hide();
   }
   
   debugger;
   var dataString = {
    checkUserSurevy:'checkUserSurevy'
  }
  $.ajax({
    type: "POST",
    url: "data.php",
    dataType: "json",
    data: dataString,
    success: function(data) {
     debugger;
     Highcharts.setOptions({
       colors: ['#07B007','#AEDA08','#FFCC0D', '#FFA008',  '#EE2016']
     }); 
     Highcharts.theme = {
      colors: ['#07B007','#AEDA08','#FFCC0D', '#FFA008',  '#EE2016'],
      chart: {
        backgroundColor: {
          linearGradient: [0, 0, 500, 500],
          stops: [
          [0, 'rgb(255, 255, 255)'],
          [1, 'rgb(240, 240, 255)']
          ]
        },
      },
      credits: {
        enabled: false
      },
      title: {
        style: {
          color: '#000',
          font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
        }
      },
      subtitle: {
        style: {
          color: '#666666',
          font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
        }
      },

      legend: {
        itemStyle: {
          font: '9pt Trebuchet MS, Verdana, sans-serif',
          color: 'black'
        },
        itemHoverStyle:{
          color: 'gray'
        }   
      }
    };

// Apply the theme
    Highcharts.setOptions(Highcharts.theme); 
    var series=data[0];
    var title1=data[0].survey    
    Highcharts.chart('allStat', {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45,
          beta: 0
        }
      },
      title: {
        text: 'Overall Statistics for '+data[0].survey
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          depth: 35,
          dataLabels: {
            enabled: true,
            format: '{point.name}'
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'Polled',
        data: data[0].userdata
      }]
    });


    Highcharts.chart('questStat', {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45,
          beta: 0
        }
      },
      title: {
        text: 'Overall Statistics for '+data[1].survey
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          depth: 35,
          dataLabels: {
            enabled: true,
            format: '{point.name}'
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'Polled',
        data: data[1].userdata
      }]
    });
  }
})
  
});
 </script>
</body>
</html>