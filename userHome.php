<?php include('header.php')?>
<div class="container-fluid" >    
   <div class="row content">
      <div class="col-sm-2 sidenav">
        <p><a href="#">Reports</a></p>
        <p><a href="#">Appointments</a></p>
    </div>
    <div class="col-sm-10 text-left"> 
        <h1>Welcomes <?php print($_SESSION["login_user"]); ?></h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <hr>
        <h3>Test</h3>
        <p>Lorem ipsum...</p>
    </div>
    <div class="modal fade" id="surveyModal" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Test Polling</h4>
          </div>
          <div class="modal-body">
              <div class="container-fluid text-center">
                <ul class="pagination pagination-sm">
                  <?php 
                  foreach($_SESSION['PollData']->Survey as $arr){ ?>
                  <li><a href="#"><?php print $arr->id; ?></a></li><?php }?>
              </ul>         
              <ul class="pager">
                  <li><a href="#">Previous</a></li>
                  <li><a href="#">Next</a></li>
              </ul>
          </div>
      </div>
  </div>
</div>
</div>
</div>
<?php include('/footer.php')?>
