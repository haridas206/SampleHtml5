<?php
session_start();  
include("db\configdb.php");
error_reporting(E_ALL ^ E_DEPRECATED);
if ($_SERVER["REQUEST_METHOD"] == "POST") {	
	
	if(isset($_POST['update']))
	{			
		$chekSql="SELECT * FROM questions_survey  WHERE questions_survey.SurveyName='".$_POST['edittedsurvey']."'"; 
		$stmt = $db->prepare($chekSql);
		$stmt->execute();
		$row = $stmt->fetchAll();		
		if(count($row)==0 || $row[0]['id'] == $_POST['surevyid'])
		{
			$d2=$_POST['edittedenddate'];
			$end_date=date("Y-m-d", strtotime($d2));

			$d1=$_POST['editedstartdate'];
			$start_date= date("Y-m-d", strtotime($d1));
			$chekDateSql="SELECT questions_survey.id FROM questions_survey join category on questions_survey.surveyDep=category.categoryValue and category.Category_Name='".$_POST['surveyName']." ' and ' ".$end_date."' and '".$start_date."' between 
			questions_survey.start_date and questions_survey.end_date";
			$stmtDate = $db->prepare($chekDateSql);
			$stmtDate->execute();
			$rowDate = $stmtDate->fetchAll();	
			if(count($rowDate)==0 || $rowDate[0]['id'] == $_POST['surevyid'])
			{
				$updateSql="UPDATE questions_survey  SET start_date = :startdate,
				end_date = :enddate,
				SurveyName = :survey
				WHERE id = :sid";
				$statement = $db->prepare($updateSql);
				$statement->bindValue(":startdate",$start_date);
				$statement->bindValue(":enddate", $end_date);
				$statement->bindValue(":survey",  $_POST['edittedsurvey']);
				$statement->bindValue(":sid",  $_POST['surevyid']);
				$count = $statement->execute();
			}
			$_SESSION['surveySuccess']='success';
			$User_Surevy_Str = "SELECT id,survey,start_date,end_date,SurveyName,surveyDep from questions_survey WHERE status='1'";
			$User_Surevy_Sql=$db->prepare($User_Surevy_Str);
			$User_Surevy_Sql->execute(); 
			$userSurveyData= $User_Surevy_Sql->fetchAll();
			echo json_encode($userSurveyData);
			exit();

		}
		else
		{
			$_SESSION['surveyfailed']='Failed';
			$User_Surevy_Str = "SELECT id,survey,start_date,end_date,SurveyName,surveyDep from questions_survey WHERE status='1'";
			$User_Surevy_Sql=$db->prepare($User_Surevy_Str);
			$User_Surevy_Sql->execute(); 
			$userSurveyData= $User_Surevy_Sql->fetchAll();
			echo json_encode($userSurveyData);
			exit();
		}
		
	}
	if(isset($_POST['remove']))
	{
		$updateSql="UPDATE questions_survey  SET status ='0' WHERE id=". $_POST['surevyid'] ;
		$Surevy_Update_Sql=$db->prepare($updateSql);
		$Surevy_Update_Sql->execute(); 
		$data= $Surevy_Update_Sql->fetchAll();
		$_SESSION['surveySuccess']='success';
		$User_Surevy_Str = "SELECT id,survey,start_date,end_date,SurveyName,surveyDep from questions_survey WHERE status='1'";
		$User_Surevy_Sql=$db->prepare($User_Surevy_Str);
		$User_Surevy_Sql->execute(); 
		$userSurveyData= $User_Surevy_Sql->fetchAll();
		echo json_encode($userSurveyData);
		exit();
	}
	if(isset($_POST['newSurvey']))
	{
		
		$d2=$_POST['surveyEnddate'];
		$end_date=date("Y-m-d", strtotime($d2));
		
		$d1=$_POST['surveyStartdate'];
		$start_date= date("Y-m-d", strtotime($d1));
		$chekSql="SELECT questions_survey.id FROM questions_survey join category on questions_survey.surveyDep=category.categoryValue and category.Category_Name='".$_POST['survey']." ' and ' ".$end_date."' and '".$start_date."' between 
		questions_survey.start_date and questions_survey.end_date  or questions_survey.SurveyName='".$_POST['newSurvey']."'"; 
		$stmt = $db->prepare($chekSql);
		$stmt->execute();
		$row = $stmt->fetchAll();		
		if(count($row)==0)
		{
			$depsql="SELECT categoryValue FROM category WHERE Category_Name='".$_POST['survey']."'";
			$stmtdep = $db->prepare($depsql);
			$stmtdep->execute();
			$rowdep = $stmtdep->fetchAll();
			$dep=$rowdep[0]['categoryValue'];
			$d=array();
			$_SESSION['surveySuccess']='success';
			$quest=array('question'=>'Template Question','id'=>'1','break_after'=>'true','required'=>'true','type'=>'single-select','isExtraComment'=>'Yes','options'=>["Yes","No"]);
			$obj=array(['Survey'=>[$quest]]);
			$surveyJson=json_encode($obj[0]);
			try{
				$statement = $db->prepare("INSERT INTO questions_survey(survey, start_date,end_date, SurveyName,surveyDep)
				                          VALUES(:survey, :sdate,:edate, :sname, :sd)");
				$statement->execute(array(
				                          "survey" => $surveyJson,
				                          "sdate" => $start_date,
				                          "edate" =>$end_date,
				                          "sname" => $_POST['newSurvey'],
				                          "sd"=>$dep
				                          ));

				header('Location: SettingPanel.php'); 
			}
			catch( PDOException $e ){
				print_r( $e );
			}
			
		}
		else
		{
			$_SESSION['surveyfailed']='Failed';
			header('Location: SettingPanel.php'); 
		}
		
	}
	else{
		try{
			$d2=$_POST['enddate'];
			$end_date=date("Y-m-d", strtotime($d2));

			$d1=$_POST['startdate'];
			$start_date= date("Y-m-d", strtotime($d1));

			if($_POST['survey'] !='All')
			{
				$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on  questions_survey.SurveyName= '".$_POST['survey']." ' and questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and tbl_surveysubmission.submitted_date between '".$start_date."' and '" .$end_date."'";

			}
			else
			{
				$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and tbl_surveysubmission.submitted_date between '".$start_date." ' and '" .$end_date."'";
			}


			$SurveyData=$db->prepare($SurveySql);
			$SurveyData->execute();
			$data_survey=$SurveyData->fetchAll();
			$d = array();
			$questions=array();
			$answers=array();
			$StatisticDataCat=array();
			$seriesData=array('Excellent','Very good','Good','Average','Poor');	
			$seriesPieData=array('Yes','No');	
			$surevyQuestionsYN=array();
			foreach($data_survey as $data)
			{
				$surveyJson=json_decode($data['submitted_survey']);			
				array_push($d, $surveyJson);
				array_push($StatisticDataCat,$data['SurveyName']);
			}
			foreach ($seriesPieData as $valuePie) 
			{

				foreach($StatisticDataCat as $pieData)
				{					
					foreach($d as $pqdata)
					{	
						foreach($pqdata as $stemp)
						{
							if($stemp->answer == $valuePie)
							{
								array_push($surevyQuestionsYN,$stemp->question);
							}
						}
					}				
				}
			}

			$surevyQuestionsYN=	array_unique($surevyQuestionsYN);
			$_session['questionCollection']=$surevyQuestionsYN;
		//print_r($surevyQuestionsYN);
			foreach($surevyQuestionsYN as  $quest)
			{
				$piedatacollection= array();
				$yescount=0;
				$nocount=0;
				foreach($d as $pqdata)
				{	
					foreach($pqdata as $stemp)
					{
						if($stemp->question == $quest)
						{
							if($stemp->answer=='Yes')
								$yescount=$yescount+1;
							if($stemp->answer=='No')
								$nocount=$nocount+1;
						}
					}
				}	
				$tempYesData= array('name'=>'Yes','y'=>$yescount);
				$tempNoData=array('name'=>'No','y'=>$nocount);
				array_push($piedatacollection,$tempYesData,$tempNoData);
				$piedate[]=['name'=>$quest,'data'=>$piedatacollection];
			}

			$StatisticDataCat=array_unique($StatisticDataCat);		
			foreach ($seriesData as $value) 
			{
				$dataStat=array();
				foreach($StatisticDataCat as $testData)
				{				
					$count=0;
					foreach($d as $sqdata)
					{	
						foreach($sqdata as $stemp)
						{
							if($stemp->answer == $value &&  $stemp->surveyName == $testData)
							{
								$count=$count+1;
							}
						}
					}
					array_push($dataStat,$count);
$data34[]=['sname'=>$testData,'qname'=>$value,'data'=>$count];
				}
				
				

			}	
			foreach($seriesData as $cal)		     
			{		 
                  $sumcal=0;				
				foreach($data34 as $puk)
				{
										
					if($puk['qname'] == $cal)
					{	
 				
						foreach($StatisticDataCat as $testData)      
						{                          					
							if($puk['sname'] == $testData)
							{
								//print  $puk['data'];
								$sumcal=$sumcal+$puk['data'] ;
								
							}
							
						}
						
					}
				}
				$totalAray[]=['name'=>$cal,'total'=> $sumcal];
			} 
			//print_r($data34);

			foreach ($totalAray as $value) 
			{
				$orgdataStat=array();
				$vals = array_count_values($value['total']);
				$totalCount=array_sum($vals);
				
				foreach($StatisticDataCat as $testData)
				{				
					foreach($data34 as $sqdata)
					{	
							if($sqdata['qname'] == $value['name'] &&  $sqdata['sname'] == $testData)
							{
								print $sqdata['data'];
								
								if($value['total'] !=0)
								array_push($orgdataStat,(($sqdata['data']/$value['total'])*totalCount));
							    else
								array_push($orgdataStat,0);
								
							}
					
					}
					
				}				

				$statCollection[]=['name'=>$value['name'],'data'=>$orgdataStat];
				

			}	
			exit();
			foreach($d as $qdata)
			{	
				foreach($qdata as $temp)
				{
					if(in_array($temp->question,$questions))
						continue;
					else
					{
						array_push($questions, $temp->question);					
					}
				}

			}
			$quesNo=0;
			foreach($questions as $ques)
			{	
				$quesNo=$quesNo+1;
				$tvalues=array();			
				foreach($d as $qdata)
				{
					foreach($qdata as $temp)
					{
						if($ques==$temp->question)	
						{				
							array_push($tvalues,$temp->answer);
						}

					}	
				}	
				$data1[]=['name'=>$ques,'data'=>$tvalues];
				$category[]=['name'=>$ques,'tooltip'=>'Q'.$quesNo,'categories'=>array_values(array_unique($tvalues))];

			}	
			$seriousValues=array();	
			foreach ($data1 as $val) {			
				$vals = array_count_values($val['data']);
				$totalCount=array_sum($vals);
			//echo 'No. of NON Duplicate Items: '.count($vals).'<br><br>';
				foreach($vals as $t)
				{
					array_push($seriousValues,(($t/$totalCount)*100));	
				}	
			//print_r($seriousValues);

			}		
			$returnData[]=['category'=>$category,'series'=>$seriousValues,'staticsCategory'=>array_values($StatisticDataCat),'staticsSeries'=>$statCollection,'pieDataCollection'=>$piedate,'questionData'=>$surevyQuestionsYN];
			echo json_encode($returnData);	
			exit();
		}     
		catch( PDOException $e ){
			print_r( $e );
		}

	}
}
if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$User_Surevy_Str = "SELECT id,survey,start_date,end_date,SurveyName,surveyDep from questions_survey WHERE status='1'";
	$User_Surevy_Sql=$db->prepare($User_Surevy_Str);
	$User_Surevy_Sql->execute(); 
	$userSurveyData= $User_Surevy_Sql->fetchAll();
	//$userSurveyData['start_date']=date("d-m-Y", strtotime($userSurveyData['start_date']));
	echo json_encode($userSurveyData);

}


?>	

