<?php
session_start();  
include("db\configdb.php");
error_reporting(E_ALL ^ E_DEPRECATED);
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	$statement = $db->prepare("UPDATE questions_survey SET survey = :survey WHERE SurveyName = :sname and status='1'");
	$statement->execute(array(
	                          "survey" => json_encode($_POST['surveyData']),
	                          "sname" => $_POST['surveyName']
	                          ));
	echo 'Success';
	exit();
}
if ($_SERVER["REQUEST_METHOD"] == "GET") {	

	$User_Surevy_Str = "SELECT id,survey,start_date,end_date,SurveyName,surveyDep from questions_survey WHERE status='1'";
	$User_Surevy_Sql=$db->prepare($User_Surevy_Str);
	$User_Surevy_Sql->execute(); 
	$userSurveyData= $User_Surevy_Sql->fetchAll();
	echo json_encode($userSurveyData);	
	exit();
}
?>