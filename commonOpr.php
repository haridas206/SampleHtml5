<?php   
session_start();
include("db\configdb.php");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$d = array();
	while ($key = current($_POST)) {    
		$str = preg_replace('/_/', ' ', key($_POST));
		#echo $str;
		#echo current($_POST).'<br />';
		$question= explode("||",$str);
		$subittePollObj= array('question'=>$question[0],'type'=>$question[1],'answer'=>current($_POST),'surveyName'=>$_SESSION['common_survey_name']);
		array_push($d, $subittePollObj);	
		next($_POST);
	}	
	$surveyJson=json_encode($d);
	
	try{
		$statement = $db->prepare("INSERT INTO tbl_surveysubmission(submitted_survey, submitted_date, user_id,survey_id)
		                          VALUES(:survey, :sdate, :uid, :sid)");
		$statement->execute(array(
		                          "survey" => $surveyJson,
		                          "sdate" => date('Y-m-d'),
		                          "uid" => $_SESSION['user_id'],
		                          "sid"=>$_SESSION['common_survey_id']
		                          ));
		$_SESSION['surveysubmitSuccess']='success';
	}
	catch( PDOException $e ){
		print_r( $e );
	}

	try {
		$updateQry = $db->prepare("UPDATE dir_users  set commonpoll = :poll WHERE user_id=:uid");
		$updateQry->execute(array(
		                          "poll"=>'1',
		                          "uid" => $_SESSION['user_id']
		                          ));
		
	} catch (PDOException $e) {
		print_r( $e );
	}
	
	header('Location: staffpolling.php');

}
?>