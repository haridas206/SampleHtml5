<?php include('./includes/header.php')?>
<div class="container-fluid">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <p><a href="#">Consult</a></p>
      <p><a href="#">Buckets</a></p>
      <p><a href="#">History</a></p>
    </div>
    <div class="col-sm-8 text-left"> 
      <h1>Healthcare University </h1>
      <p>At the University of Iowa, we’re all creators. From tomorrow’s doctors and engineers, to poets and playwrights, to physicists and entrepreneurs, every single Hawkeye learns how to build their own path and bravely go wherever it leads</p>
      <hr>
      <h3>News</h3>
      <p>"The future is our never-ending frontier...and we're always moving forward."</p>
    </div>
    <div class="col-sm-2 sidenav">
      <div class="well">
        <p>News 1</p>
      </div>
      <div class="well">
        <p>News 2</p>
      </div>
    </div>
  </div>
  <div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body">
         <form action="./Home.php">
           <div class="form-group">
             <label for="inputdefault">Name</label>
             <input class="form-control" name='login' id="inputdefault" type="text">
           </div>    
           <div class="form-group">
             <label for="sel3">Department</label>
             <select class="form-control input-sm" name="dep" id="sel3">
               <option>D</option>
               <option>N</option>
               <option>S</option>
               <option>AD</option>
             </select>
           </div>
           <button type="submit" class="btn btn-primary">Start</button>
         </form>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>

<?php include('./includes/footer.php')?>
<style>
  .container-fluid{margin:0 15px;}
</style>