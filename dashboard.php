<!DOCTYPE html>

<script>

 function switchVisible() {

  document.getElementById('Div1').style.display = 'block';

}

</script>
<head>
  <meta charset="utf-8">
  <title>EMPLOYEE PORTAL| DASHBOARD</title>
  <script src="js/html5-trunk.js"></script>
  <link href="icomoon/style.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">  
</head>
<body>
  <style>
    .panel-group .panel {
      border-radius: 0;
      box-shadow: none;
      border-color: #EEEEEE;
    }

    .panel-default > .panel-heading {
      padding: 0;
      border-radius: 0;
      color: #212121;
      background-color: #FAFAFA;
      border-color: #EEEEEE;
    }

    .panel-title {
      font-size: 14px;
    }

    .panel-title > a {
      display: block;
      padding: 15px;
      text-decoration: none;
    }

    .more-less {
      float: right;
      color: #212121;
    }

    .panel-default > .panel-heading + .panel-collapse > .panel-body {
      border-top-color: #EEEEEE;
    }

    .form-group {
      display: inline-block;
      margin-bottom: 0;
    }
    .row {
      margin-right: 0pxpx;
      margin-left: 0px;
    }

    #fieldsetDiv {
      padding: 0;
      margin: 0;
      border: 0;
      padding-left: 10px;
      vertical-align: bottom;
    }
    #DivFetch{
      display: inline-block;
      margin-bottom: 0;
      vertical-align: bottom;
    }
    
  </style>
  <?php include('./includes/header.php');
  try{
    $surveySql="SELECT id,SurveyName FROM questions_survey WHERE status='1'";
    $survey=$db->prepare($surveySql);
    $survey->execute();
    $row_survey=$survey->fetchAll();
    $_SESSION['survey_items']=$row_survey;
  }     
  catch( PDOException $e ){
    print_r( $e );
  }

  if ($_SESSION['view_Dashborad'] =="0"){

    echo "<style>
#divChart {
    display: none;
  }
</style>";
}
else
{
  echo "<style>
  #DivNoPermission {
  display: none;
}
</style>";
}
?>

<div class="container-fluid">
    <!-- Navigation ----------------------------------------------------------------Navigation-----------------------Navigation --------->
    <div id="mainnav" class="hidden-phone hidden-tablet">
      <ul>
        <li class="active">
          <span class="current-arrow"></span>
          <a href="">
            <div class="icon" id='dashdiv'>
              <span class="fs1" aria-hidden="true" data-icon="&#xe0a1;"></span>           
            Dashboard
			 </div>
          </a>
        </li>
        <li>
          <a href="my profile.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe071;"></span>
            </div>
            My Profile
          </a>
        </li>
        <li>
          <a href="Whatsup.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe1b3;"></span>
            </div>
            WHATSAPP
          </a>
        </li>
        <li>
          <a href="staffpolling.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            STAFF POLLING
          </a>
        </li>
        <br/>
        <li>        
          <a href="SettingPanel.php">
            <div class="fs1" id="SetDiv">
              <span class="icon-cog-2" aria-hidden="true" data-icon="&#xe994"></span>
			  SETTINGS
            </div>
            
          </a>
        </li>
      </ul>
    </div>
    <div class="dashboard-wrapper">
      <div class="main-container">
        <!----------------------------------------------------navigationMini------------------------------------------------------------------------- -->
        <div class="navbar hidden-desktop">
          <div class="navbar-inner">
            <div class="container">
              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
                  <li>
                    <a href="dashboard.php">Dashboard</a>
                  </li>
                  <li>
                    <a href="my profile.php">My Profile</a>
                  </li>
                  <li>
                    <a href="whatsup.php">What's Up</a>
                  </li>
                  <li>
                    <a href="#">Staff Polling</a>
                  </li>
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12">
            <ul class="breadcrumb-beauty">
              <li>
                <!-------------------- EDIT HERE DASHBOARD ---------------------->
                <h4 id="dashBoardLabel" class='text-primary'><span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span> Dashboard</h4>              
              </li>
              <li>
              </li>
            </ul>

            <div class="row-fluid " id="divChart">
              <div class="span12">
                <div class="widget no-margin">
                  <div class="widget-header">
                    <div class="title">
                      <span class="fs1" aria-hidden="true" data-icon=""></span> Chart   
                    </div>
                    <div class="tools pull-right">
                    </div>          
                  </div>
                  <div class="widget-body">
                    <div id="content">                   
                      <div class="row form-inline">
                        <form action = "data.php" method = "post">
                          <fieldset id='fieldsetDiv'>
                            <div class="form-group"> <!-- Date input -->
                             <label class="control-label" for="date">Start Date</label>
                             <input class="form-control" id="startdate" name="startdate" required="true"   placeholder="DD-MM-YYYY" type="text"/>

                           </div>
                           <div class="form-group"> <!-- Date input -->
                             <label class="control-label" for="enddate">End Date</label>
                             <input class="form-control" id="enddate" name="enddate" required="true" placeholder="DD-MM-YYYY" type="text"/>
                           </div>
                           <div class="form-group"> <!-- Date input -->
                             <label class="control-label" for="enddate">Based on</label>
                             <select class="form-control input-sm" name="survey" id="sel3">
                              <option>All</option>
                              <?php foreach($_SESSION['survey_items'] as $survey)
                              {?>
                              <option value="<?php print  $survey['SurveyName']; ?>"><?php print  $survey['SurveyName'] ; ?></option>
                              <?php } 
                              ?>                                   
                            </select>
                          </div>
                          <div class="form-group"> <!-- Date input -->
                            <label class="control-label" for="enddate">Based on Gender</label>
                            <select class="form-control input-sm" name="genderBox" id="genderBox">
                              <option value="all">All</option> 
                              <option value="1">Male</option>                                 
                              <option value="0">Female</option> 
                            </select>
                          </div>
                          <div class="form-group"> <!-- Date input -->
                           <label class="control-label" for="enddate">Based on Age</label>
                           <select class="form-control input-sm" name="ageBox" id="ageBox">
                            <option value="all">All</option> 
                            <option value="1">Below 30 Years</option>                                 
                            <option value="2">30-40 Years</option> 
                            <option value="3">40 Years Above</option>
                          </select>
                        </div>
                        <div id='DivFetch' class="form-group"> <!-- Date input -->
                         <button class="btn btn-primary " name="submit" id="subBtn" type='button'>Update</button>		
                       </div>						  

                     </fieldset>
                   </form>
                 </div>					
               </div>

               <!--content-->
               <div class="container" hidden="true" id='DivNoData'>
                <div class="col-md-12">
                  <!-- Horizontal Form -->
                  <h1>No Data Available </h1>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="row-fluid"  id="DivNoPermission">
        <div class="span12">
          <div>
            <div class="widget no-margin">
              <div class="widget-header">
                <div class="icontype">

                </div>
                <div class="title">
                  <span class="fs1" aria-hidden="true" data-icon=""></span> Warning 
                </div>
              </div>
              <div class="widget-body">
                <p class="text-primary"> No Permission to access this page</p>
              </div>
            </div> 
          </div>
        </div>
      </div>  
    </div>
  </div>

</div><!-- dashboard-container -->
<div class="container">
  <div class="row">
    <div class='col-sm12'>
      <div class='col-sm-6'>
        <br/>
        <div id="allStat" style="min-width: 310px; height: 400px; margin: 0 auto"> </div>
      </div>
      <div class='col-sm-6'>
        <div id='optDiv' hidden="true">
          <label class="control-label">Based on</label>
          <select class="form-control input-sm" name="survey" id="selQues">  
          </select>
        </div>            
        <div id="questStat" style="min-width: 310px; height: 400px; margin: 0 auto"> </div>
      </div>
    </div>

  </div>
</div>
<div class="container">
  <div class="row">
    <div class='col-sm12'>   
      <div id='optDivMain' hidden="true">
        <label class="control-label">Based on</label>
        <select class="form-control input-sm" name="Excellentsurvey" id="selQuesExcellent">  
        </select>
      </div>            
      <div id="StatAllquest" style="min-width: 310px; height: 400px; margin: 0 auto"> </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="panel panel-success" id='accordianDiv' hidden='true'>
      <div class="panel-header with-border">
       <h2 class="panel-title text-center">Comments</h2>
     </div>
     <div class="panel-body">

       <div class="panel-group" id="accordion" role="tablist"
       aria-multiselectable="true">
     </div>

   </div>
 </div>
</div>
</div>

</div>
</div>
<!-- container-fluid -->
<?php include('./includes/footer.php')?>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.js"></script>

<!-- Calendar Js -->
<script src="js/fullcalendar.js"></script>
<!-- Tiny Scrollbar JS -->
<script src="js/tiny-scrollbar.js"></script>   

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="./js/grouped-categories.js"></script>
<script src="./js/themes/gray.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">


</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>

  $(document).ready(function(){
    var e = window.event;

    var $template = $(".template");
    var wrapper = $("#accordion");
    var commentQuestions=[];
    var commentDataCollection=[];
    var hash = 2;
    var pieChartData=[];
    var allPieChartData=[];
      var startdate_input=$('input[name="startdate"]'); //our date input has the name "date"
      var enddate_input=$('input[name="enddate"]'); 
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      startdate_input.datepicker({
        format: 'dd-mm-yyyy',
        container: container,
        todayHighlight: false,
        autoclose: true,
        selected:true,
      })
      enddate_input.datepicker({
        format: 'dd-mm-yyyy',
        container: container,
        todayHighlight: false,
        autoclose: true,
      })
      $("#selQuesExcellent").change(function(){   
        $(this).find("option:selected").each(function(){
          var optionValue = $(this).attr("value");
          for (var i = 0; i<allPieChartData.length; i++){
            if(optionValue==allPieChartData[i].name)
            {
              var chartData=allPieChartData[i];
            }
          }
          debugger;
          Highcharts.chart('StatAllquest', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            title: {
              text: 'Polling Based on All Question'
            },
            tooltip: {
              pointFormat: '{point.percentage:.1f}%</b>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: false
                },
                showInLegend: true
              }
            },
            series: [chartData]
          });      
        });
      }).change();
      $("#selQues").change(function(){   
        $(this).find("option:selected").each(function(){
          var optionValue = $(this).attr("value");
          for (var i = 0; i<pieChartData.length; i++){
            if(optionValue==pieChartData[i].name)
            {
              var chartData=pieChartData[i];
            }
          }
          debugger;
          Highcharts.chart('questStat', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            title: {
              text: 'Statisfaction Chart'
            },
            tooltip: {
              pointFormat: '{point.percentage:.1f}%</b>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: false
                },
                showInLegend: true
              }
            },
            series: [chartData]
          });      
        });
      }).change();
      $("#subBtn").click(function() {
       var dataString = {
        startdate: document.getElementById('startdate').value,
        enddate: document.getElementById('enddate').value,
        survey: document.getElementById('sel3').value,
        gender:document.getElementById('genderBox').value,
        age:document.getElementById('ageBox').value
      }

      $.ajax({
        type: "POST",
        url: "data.php",
        dataType: "json",
        data: dataString,
        success: function(data) {
         debugger;
         if(data[0].category[0]=='Failed')
         {
           var acc = document.getElementById('accordion');
           acc.innerHTML='';
           var qest = document.getElementById('questStat');
           qest.innerHTML='';
           var allStat = document.getElementById('allStat');
           allStat.innerHTML='';
           var allExStat = document.getElementById('StatAllquest');
           allExStat.innerHTML='';           
           $("#optDiv").hide();             
           $("#optDivMain").hide()       
           $("#DivNoData").show();
           $("#dashBoardLabel")[0].innerHTML ='<span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span>Dashboard';
           

         }
         else
         {
          $("#optDiv").show();
          $("#optDivMain").show() 
          $("#DivNoData").hide();
          var acc = document.getElementById('accordion');
          acc.innerHTML='';
          $("#dashBoardLabel")[0].innerHTML ='';
          $("#dashBoardLabel")[0].innerHTML ='<span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span>Dashboard(Total Survey: '+data[0].totalCount+')';
          commentQuestion=data[0].commentQuestion;
          commentDataCollection=data[0].customDataCollection;
          $("#accordianDiv").show();

          for(var i=0;i<data[0].commentQuestion.length;i++)
          {
           var ariaExpanded = false;
           var expandedClass = '';
           var collapsedClass = 'collapsed';
           if(i==0){
             ariaExpanded = true;
             expandedClass = 'in';
             collapsedClass = '';
           }
           $(wrapper).append('<div id="mainSec" class="col-sm-12" style="margin-bottom: 0;"><div class="panel panel-default" id="panel'+ i +'">' + 
                             '<div class="panel-heading" role="tab" id="heading'+ i +'"><h4 class="panel-title">' +
                             '<a class="'+collapsedClass+'" id="panel-lebel'+ i +'" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+ i +'" ' +
                             'aria-expanded="'+ariaExpanded+'" aria-controls="collapse'+ i +'"> '+data[0].commentQuestion[i]+' </a><div class="actions_div" style="position: relative; top: -26px;">' +
                             '<a href="#" accesskey="'+ i +'" class="edit_ctg_label pull-right">' +
                             '<a href="#" accesskey="'+ i +'" class="pull-right" id="addButton2"> </a></div></h4></div>' +
                             '<div id="collapse'+ i +'" class="panel-collapse collapse '+expandedClass+'"role="tabpanel" aria-labelledby="heading'+ i +'">'+
                             '<div class="panel-body"><div style="overflow-x:auto;"><table id="table'+i+'" style="border-collapse: collapse;width: 99%;"><tr><th style=" background-color: #4CAF50;'+
                             'color: white;">S.NO</th><th style=" background-color: #4CAF50;'+
                             'color: white;">Comment</th><th style=" background-color: #4CAF50;color: white;">Commented By</th><th  style=" background-color: #4CAF50;color: white;">Survey</th></tr>' +
                             '</table></div></div></div></div></div>');
           for(var j=0;j<data[0].customDataCollection.length;j++)
           {
             if(data[0].customDataCollection[j].question==data[0].commentQuestion[i])
             {
               for(var k=0;k<data[0].customDataCollection[j].datacollection.length;k++)
               {
                var tbdata='<tr><td style="border: 1px solid #ddd;padding: 6px;">'+Number(k+1)+'</td><td style="border: 1px solid #ddd;padding: 6px;">'+data[0].customDataCollection[j].datacollection[k].comment+'</td><td style="border: 1px solid #ddd;padding: 6px;">'+data[0].customDataCollection[j].datacollection[k].user+'</td><td style="border:1px solid #ddd;padding: 6px;">'+data[0].customDataCollection[j].datacollection[k].surveyName+'</td></tr>'
                $('#table'+i).append(tbdata);
              }

            }
          }
				  //if()


        }
        Highcharts.setOptions({
         colors: ['#07B007','#AEDA08','#FFCC0D', '#FFA008',  '#EE2016']
       }); 
        Highcharts.theme = {
          colors: ['#07B007','#AEDA08','#FFCC0D', '#FFA008',  '#EE2016'],
          chart: {
            backgroundColor: {
              linearGradient: [0, 0, 500, 500],
              stops: [
              [0, 'rgb(255, 255, 255)'],
              [1, 'rgb(240, 240, 255)']
              ]
            },
          },
          credits: {
            enabled: false
          },
          title: {
            style: {
              color: '#000',
              font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
            }
          },
          subtitle: {
            style: {
              color: '#666666',
              font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
            }
          },

          legend: {
            itemStyle: {
              font: '9pt Trebuchet MS, Verdana, sans-serif',
              color: 'black'
            },
            itemHoverStyle:{
              color: 'gray'
            }   
          }
        };

// Apply the theme
        Highcharts.setOptions(Highcharts.theme); 
        var series=data[0].series;



        Highcharts.chart('allStat', {

          chart: {
            type: 'column'
          },
          title: {
            text: 'Overall Statistics Chart'
          },
          xAxis: {
            categories: data[0].staticsCategory

          },
          yAxis: {
           allowDecimals: false,
           min: 0,
           max:100,
           title: {
            text: 'Polling Percentage'
          }
        },
        tooltip: {		
          positioner: function(w, h, p) {
            return { x:200, y:25 };
          },
          pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y:.1f}%</b><br/>',
          shared: true
        },
        plotOptions: {
          column: {
            pointPadding: .2,
            borderWidth: 0 ,  

          }
        },
        series: data[0].staticsSeries
      }); 
        if(data[0].pieAllStatData.length>0 && data[0].pieAllStatData[0] !='')
        {
          $("#optDivMain").show()
          var allSelect = document.getElementById('selQuesExcellent');
          allPieChartData=data[0].pieAllStatData
          allSelect.innerHTML = '';
          for (var i = 0; i<data[0].pieAllStatData.length; i++){
            var opt = document.createElement('option');
            opt.value =data[0].pieAllStatData[i].name;
            opt.innerHTML = data[0].pieAllStatData[i].name;
            allSelect.appendChild(opt);
          } 
          Highcharts.chart('StatAllquest', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            title: {
              text: 'Polling Based on All Question'
            },
            tooltip: {
              pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: false
                },
                showInLegend: true
              }
            },
            series: [data[0].pieAllStatData[0]]
          }); 
        }
        else{
          $("#optDivMain").hide()
        }           

        if(data[0].pieDataCollection.length>0 && data[0].pieDataCollection[0] !='')
        {
          debugger;
          $("#optDiv").show()
          var select = document.getElementById('selQues');
          pieChartData=data[0].pieDataCollection
          select.innerHTML = '';
          for (var i = 0; i<data[0].pieDataCollection.length; i++){
            var opt = document.createElement('option');
            opt.value =data[0].pieDataCollection[i].name;
            opt.innerHTML = data[0].pieDataCollection[i].name;
            select.appendChild(opt);
          }
          Highcharts.chart('questStat', {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            title: {
              text: 'Polling Based on Open Question'
            },
            tooltip: {
              pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: false
                },
                showInLegend: true
              }
            },
            series: [data[0].pieDataCollection[0]]
          }); 
        }
        else{
          $("#optDiv").hide()
        }           
      } 
    }  
  });
    /*$.post("./data.php",data, function(json,status){
      alert("Data: " + json + "\nStatus: " + status);
    }     });*/


  });


})
</script>
</body>
</html>
