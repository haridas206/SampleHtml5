<?php 
session_start();  
include("db\configdb.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Polling</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
        margin-bottom: 0;
        border-radius: 0;
    }

    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}

    /* Set gray background color and 100% height */
    .sidenav {
        padding-top: 20px;
        background-color: #f1f1f1;
        height: 100%;
    }

    /* Set black background color, white text and some padding */
    footer {
        background-color: #555;
        color: white;
        padding: 15px;
    }

    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
        .sidenav {
            height: auto;
            padding: 15px;
        }
        .row.content {height:auto;} 
    }
</style>
</head>
<body>

  <nav class="navbar navbar-inverse">
    <div class="container-fluid text-center">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
      </button>
      <a href="#" class="logo"><img alt="Brand" src="img/logo.png"></a>
  </div>
  <div class="collapse navbar-collapse" id="myNavbar">
    <ul class="nav navbar-nav">       
      <li class="active"><a href="index.php">Home</a></li>       
  </ul>
  <ul class="nav navbar-nav navbar-right">
      <?php if(!isset($_SESSION["login_user"]) || $_SESSION["login_user"] == ''){ ?>
      <li  id="login"  data-toggle="modal" data-target="#loginModal"><a href=""><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      <?php } 
      else{ ?>
      <li><a href="#">Hi ,<?php echo $_SESSION['login_user'];?> </a></li>
      <li class="dropdown">
      </li>
      <li  id="logout" ><a href="./Home.php?logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li><?php } ?>  
  </ul>
</div>
</div>
</nav>
