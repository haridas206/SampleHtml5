<?php
session_start();  
include("db\configdb.php");
error_reporting(E_ALL ^ E_DEPRECATED);
if ($_SERVER["REQUEST_METHOD"] == "POST") {	
	
	if(isset($_POST['update']))
	{			
		$chekSql="SELECT * FROM questions_survey  WHERE questions_survey.SurveyName='".$_POST['edittedsurvey']."'"; 
		$stmt = $db->prepare($chekSql);
		$stmt->execute();
		$row = $stmt->fetchAll();		
		if(count($row)==0 || $row[0]['id'] == $_POST['surevyid'])
		{
			$d2=$_POST['edittedenddate'];
			$end_date=date("Y-m-d", strtotime($d2));

			$d1=$_POST['editedstartdate'];
			$start_date= date("Y-m-d", strtotime($d1));
			$chekDateSql="SELECT questions_survey.id FROM questions_survey join category on questions_survey.surveyDep=category.categoryValue and category.Category_Name='".$_POST['surveyName']." ' and ' ".$end_date."' and '".$start_date."' between 
			questions_survey.start_date and questions_survey.end_date";
			$stmtDate = $db->prepare($chekDateSql);
			$stmtDate->execute();
			$rowDate = $stmtDate->fetchAll();	
			if(count($rowDate)==0 || $rowDate[0]['id'] == $_POST['surevyid'])
			{
				$updateSql="UPDATE questions_survey  SET start_date = :startdate,
				end_date = :enddate,
				SurveyName = :survey
				WHERE id = :sid";
				$statement = $db->prepare($updateSql);
				$statement->bindValue(":startdate",$start_date);
				$statement->bindValue(":enddate", $end_date);
				$statement->bindValue(":survey",  $_POST['edittedsurvey']);
				$statement->bindValue(":sid",  $_POST['surevyid']);
				$count = $statement->execute();
			}
			$_SESSION['surveySuccess']='success';
			$User_Surevy_Str = "SELECT id,survey,start_date,end_date,SurveyName,surveyDep from questions_survey WHERE status='1'";
			$User_Surevy_Sql=$db->prepare($User_Surevy_Str);
			$User_Surevy_Sql->execute(); 
			$userSurveyData= $User_Surevy_Sql->fetchAll();
			echo json_encode($userSurveyData);
			exit();

		}
		else
		{
			$_SESSION['surveyfailed']='Failed';
			$User_Surevy_Str = "SELECT id,survey,start_date,end_date,SurveyName,surveyDep from questions_survey WHERE status='1'";
			$User_Surevy_Sql=$db->prepare($User_Surevy_Str);
			$User_Surevy_Sql->execute(); 
			$userSurveyData= $User_Surevy_Sql->fetchAll();
			echo json_encode($userSurveyData);
			exit();
		}
		
	}
	if(isset($_POST['remove']))
	{
		$updateSql="UPDATE questions_survey  SET status ='0' WHERE id=". $_POST['surevyid'] ;
		$Surevy_Update_Sql=$db->prepare($updateSql);
		$Surevy_Update_Sql->execute(); 
		$data= $Surevy_Update_Sql->fetchAll();
		$_SESSION['surveySuccess']='success';
		$User_Surevy_Str = "SELECT id,survey,start_date,end_date,SurveyName,surveyDep from questions_survey WHERE status='1'";
		$User_Surevy_Sql=$db->prepare($User_Surevy_Str);
		$User_Surevy_Sql->execute(); 
		$userSurveyData= $User_Surevy_Sql->fetchAll();
		echo json_encode($userSurveyData);
		exit();
	}
	
	if(isset($_POST['newSurvey']))
	{
		
		$d2=$_POST['surveyEnddate'];
		$end_date=date("Y-m-d", strtotime($d2));
		
		$d1=$_POST['surveyStartdate'];
		$start_date= date("Y-m-d", strtotime($d1));
		$chekSql="SELECT questions_survey.id FROM questions_survey join category on questions_survey.surveyDep=category.categoryValue and category.Category_Name='".$_POST['survey']." ' and ' ".$end_date."' and '".$start_date."' between 
		questions_survey.start_date and questions_survey.end_date  or questions_survey.SurveyName='".$_POST['newSurvey']."'"; 
		$stmt = $db->prepare($chekSql);
		$stmt->execute();
		$row = $stmt->fetchAll();		
		if(count($row)==0)
		{
			$depsql="SELECT categoryValue FROM category WHERE Category_Name='".$_POST['survey']."'";
			$stmtdep = $db->prepare($depsql);
			$stmtdep->execute();
			$rowdep = $stmtdep->fetchAll();
			$dep=$rowdep[0]['categoryValue'];
			$d=array();
			$_SESSION['surveySuccess']='success';
			$quest=array('question'=>'Template Question','id'=>'1','break_after'=>'true','required'=>'true','type'=>'single-select','isExtraComment'=>'Yes','options'=>["Yes","No"]);
			$obj=array(['Survey'=>[$quest]]);
			$surveyJson=json_encode($obj[0]);
			try{
				$statement = $db->prepare("INSERT INTO questions_survey(survey, start_date,end_date, SurveyName,surveyDep)
				                          VALUES(:survey, :sdate,:edate, :sname, :sd)");
				$statement->execute(array(
				                          "survey" => $surveyJson,
				                          "sdate" => $start_date,
				                          "edate" =>$end_date,
				                          "sname" => $_POST['newSurvey'],
				                          "sd"=>$dep
				                          ));

				header('Location: SettingPanel.php'); 
			}
			catch( PDOException $e ){
				print_r( $e );
			}
			
		}
		else
		{
			$_SESSION['surveyfailed']='Failed';
			header('Location: SettingPanel.php'); 
		}
		
	}
	if(isset($_POST['checkUserSurevy']))
	{
		
		$todaydate=date("Y-m-d");
		$usrSurveySql="SELECT tbl_surveysubmission.submitted_survey,questions_survey.SurveyName FROM  tbl_surveysubmission join questions_survey  on questions_survey.id= tbl_surveysubmission.survey_id and (questions_survey.surveyDep='".$_SESSION['user_dep']."' OR questions_survey.surveyDep='CM')  and '".$todaydate."' between questions_survey.start_date and questions_survey.end_date";
		
		$utmt = $db->prepare($usrSurveySql);
		$utmt->execute();
		$row = $utmt->fetchAll();
		$userSurveyDataCollection=array();
		$userSurveyArr=array();
		$d1=array();
		$userseriesData=array('Excellent','Very Good','Good','Average','Poor');	
		if(count($row) !=0)	
		{		
			foreach($row as $data)
			{
				$usersurveyJson=json_decode($data['submitted_survey']);	
				array_push($d1, $usersurveyJson);	
				array_push($userSurveyArr,$data['SurveyName']);

			}
			$userSurveyArr=array_unique($userSurveyArr);
			foreach($userSurveyArr as $userSur)
			{
				$userpiedatacollection=array();
				foreach ($userseriesData as $valuePie) 
				{
					$count=0;								
					foreach($d1 as $pqdata)
					{	
						foreach($pqdata as $stemp)
						{
							if($stemp->answer == $valuePie &&($stemp->type == null || $stemp->type == 'single-select') && $stemp->surveyName==$userSur)
							{
								$count=$count+1;
							}
						}
					}			
					$usertempData=array('name'=>$valuePie,'y'=>$count);
					array_push($userpiedatacollection,$usertempData);
				}
				$userSurveyDataCollection[]=['survey'=>$userSur,'userdata'=>$userpiedatacollection];
			}

			echo json_encode($userSurveyDataCollection);
			exit();
		}
		exit();
	}
	else{
		try{
			$d2=$_POST['enddate'];
			$end_date=date("Y-m-d", strtotime($d2));

			$d1=$_POST['startdate'];
			$start_date= date("Y-m-d", strtotime($d1));
			$gender=$_POST['gender'];
			$age=$_POST['age'];
			
			if($_POST['survey'] !='All')
			{
				if($gender == 'all')
				{
					
					if($age == 'all')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on  questions_survey.SurveyName= '".$_POST['survey']." ' and questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id  and tbl_surveysubmission.submitted_date between '".$start_date."' and '" .$end_date."'";
					}
					else if($age == '1')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on  questions_survey.SurveyName= '".$_POST['survey']." ' and questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id  and tbl_surveysubmission.submitted_date between '".$start_date."' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) =< 30 ";
					}
					else if($age == '2')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on  questions_survey.SurveyName= '".$_POST['survey']." ' and questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id  and tbl_surveysubmission.submitted_date between '".$start_date."' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) between 30 and 40";
					}
					else
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on  questions_survey.SurveyName= '".$_POST['survey']." ' and questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id  and tbl_surveysubmission.submitted_date between '".$start_date."' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) >= 40 ";
					}
				}
				else
				{
					if($age == 'all')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,dir_users.user_gender,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on  questions_survey.SurveyName= '".$_POST['survey']." ' and questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and dir_users.user_gender='".$gender."' and tbl_surveysubmission.submitted_date between '".$start_date."' and '" .$end_date."'";
					}
					else if($age == '1')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,dir_users.user_gender,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on  questions_survey.SurveyName= '".$_POST['survey']." ' and questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and dir_users.user_gender='".$gender."' and tbl_surveysubmission.submitted_date between '".$start_date."' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) =< 30 ";
					}
					else if($age == '2')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,dir_users.user_gender,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on  questions_survey.SurveyName= '".$_POST['survey']." ' and questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and dir_users.user_gender='".$gender."' and tbl_surveysubmission.submitted_date between '".$start_date."' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) between 30 and 40";
					}
					else
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,dir_users.user_gender,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on  questions_survey.SurveyName= '".$_POST['survey']." ' and questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and dir_users.user_gender='".$gender."' and tbl_surveysubmission.submitted_date between '".$start_date."' and '" .$end_date."'and floor(datediff(curdate(),dir_users.dob) / 365) >= 40 ";
					}
					
				}


			}
			else
			{
				if($gender == 'all')
				{
					
					if($age == 'all')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and tbl_surveysubmission.submitted_date between '".$start_date." ' and '" .$end_date."'";
					}
					else if($age == '1')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and tbl_surveysubmission.submitted_date between '".$start_date." ' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) =< 30 ";
					}
					else if($age == '2')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and tbl_surveysubmission.submitted_date between '".$start_date." ' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) between 30 and 40";
					}
					else
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and tbl_surveysubmission.submitted_date between '".$start_date." ' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) >= 40 ";
					}
				}
				else
				{
					
					if($age == 'all')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,dir_users.user_gender,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and dir_users.user_gender='".$gender."' and tbl_surveysubmission.submitted_date between '".$start_date." ' and '" .$end_date."'";
					}
					else if($age == '1')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,dir_users.user_gender,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and dir_users.user_gender='".$gender."' and tbl_surveysubmission.submitted_date between '".$start_date." ' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) =< 30 ";
					}
					else if($age == '2')
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,dir_users.user_gender,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and dir_users.user_gender='".$gender."' and tbl_surveysubmission.submitted_date between '".$start_date." ' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) between 30 and 40";
					}
					else
					{
						$SurveySql="SELECT tbl_surveysubmission.submitted_survey,dir_users.user_name,dir_users.user_gender,questions_survey.SurveyName FROM tbl_surveysubmission join dir_users join questions_survey on questions_survey.id=tbl_surveysubmission.survey_id and tbl_surveysubmission.user_id=dir_users.user_id and dir_users.user_gender='".$gender."' and tbl_surveysubmission.submitted_date between '".$start_date." ' and '" .$end_date."' and floor(datediff(curdate(),dir_users.dob) / 365) >= 40 ";
					}
				}
			}


			$SurveyData=$db->prepare($SurveySql);
			$SurveyData->execute();
			$data_survey=$SurveyData->fetchAll();
			if(count($data_survey)==0)
			{
				$returnData[]=['category'=>['Failed']];
				echo json_encode($returnData);	
				exit();
			}
			$totalSuveyCount=count($data_survey);
			$d = array();
			$questions=array();
			$customquestions=array();
			$answers=array();
			$piedate=array();
			$pieAllStatdata=array();
			$StatisticDataCat=array();
			$seriesData=array('Excellent','Very Good','Good','Average','Poor');	
			$seriesPieData=array('Yes','No');	
			$surevyQuestionsYN=array();
			$surevyQuestionsAllStat=array();
			$surveyColl=array();
			foreach($data_survey as $data)
			{
				$surveyJson=json_decode($data['submitted_survey']);			
				array_push($d, $surveyJson);
				$commentDataCollection[]=['data'=>$surveyJson,'user'=>$data['user_name'],'sname'=>$data['SurveyName']];
				array_push($StatisticDataCat,$data['SurveyName']);
			}
			$surveyColl=array_unique($StatisticDataCat);
			foreach($StatisticDataCat as $commentData)
			{					
				foreach($commentDataCollection as $pqdata)
				{	
					foreach($pqdata['data'] as $stemp)
					{
						if($stemp->type == 'Custom-Text')
						{
							array_push($customquestions,$stemp->question);

						}
					}
				}				
			}
			$customquestions=array_values(array_unique($customquestions));
			$customGridJson=array();
			foreach($customquestions as $csq)
			{
				$customGridCollection=null;
				foreach($commentDataCollection as $pqdata)
				{	
					foreach($pqdata['data'] as $stemp)
					{
						if($stemp->question == $csq)
						{
							$customGridCollection[]=['comment'=>$stemp->answer,'user'=>$pqdata['user'],'surveyName'=>$pqdata['sname']];

						}
					}
				}
				$customGridJson[]=['question'=>$csq,'datacollection'=> $customGridCollection];				
			}
			foreach ($seriesData as $valueAllPie) 
			{

				foreach($StatisticDataCat as $pieAllData)
				{					
					foreach($d as $pqdata)
					{	
						foreach($pqdata as $stemp)
						{
							if($stemp->answer == $valueAllPie && $stemp->type == 'single-select')
							{
								array_push($surevyQuestionsAllStat,$stemp->question);
							}
						}
					}				
				}
			}

			foreach ($seriesPieData as $valuePie) 
			{

				foreach($StatisticDataCat as $pieData)
				{					
					foreach($d as $pqdata)
					{	
						foreach($pqdata as $stemp)
						{
							if($stemp->answer == $valuePie && $stemp->type == 'single-select')
							{
								array_push($surevyQuestionsYN,$stemp->question);
							}
						}
					}				
				}
			}
			$surevyQuestionsAllStat=array_unique($surevyQuestionsAllStat);
			$surevyQuestionsYN=	array_unique($surevyQuestionsYN);
			$_session['questionCollection']=$surevyQuestionsYN;

		//print_r($surevyQuestionsYN);
			foreach($surevyQuestionsAllStat as  $questAll)
			{
				$pieAlldatacollection= array();
				$Excellentcount=0;
				$VeryGoodcount=0;
				$Goodcount=0;
				$AverageCount=0;
				$PoorCount=0;
				foreach($d as $pqdata)
				{	
					foreach($pqdata as $stemp)
					{
						if($stemp->question == $questAll && $stemp->type == 'single-select')
						{
							if($stemp->answer=='Excellent')
								$Excellentcount=$Excellentcount+1;
							if($stemp->answer=='Very Good')
								$VeryGoodcount=$VeryGoodcount+1;
							if($stemp->answer=='Good')
								$Goodcount=$Goodcount+1;
							if($stemp->answer=='Average')
								$AverageCount=$AverageCount+1;
							if($stemp->answer=='Poor')
								$PoorCount=$PoorCount+1;
						}

					}
				}	
				$tempExData= array('name'=>'Excellent','y'=>$Excellentcount);
				$tempVGData=array('name'=>'Very Good','y'=>$VeryGoodcount);
				$tempGData=array('name'=>'Good','y'=>$Goodcount);
				$tempAvgData=array('name'=>'Average','y'=>$AverageCount);
				$tempPoorData=array('name'=>'Poor','y'=>$PoorCount);

				array_push($pieAlldatacollection,$tempExData,$tempVGData,$tempGData,$tempAvgData,$tempPoorData);

				$pieAllStatdata[]=['name'=>$questAll,'data'=>$pieAlldatacollection];
			}
			if($pieAllStatdata == NULL)
			{
				$pieAllStatdata[]='';
			}
			foreach($surevyQuestionsYN as  $quest)
			{
				$piedatacollection= array();
				$yescount=0;
				$nocount=0;
				foreach($d as $pqdata)
				{	
					foreach($pqdata as $stemp)
					{
						if($stemp->question == $quest && $stemp->type == 'single-select')
						{
							if($stemp->answer=='Yes')
								$yescount=$yescount+1;
							if($stemp->answer=='No')
								$nocount=$nocount+1;
						}

					}
				}	
				$tempYesData= array('name'=>'Yes','y'=>$yescount);
				$tempNoData=array('name'=>'No','y'=>$nocount);
				array_push($piedatacollection,$tempYesData,$tempNoData);
				$piedate[]=['name'=>$quest,'data'=>$piedatacollection];
			}
			if($piedate == NULL)
			{
				$piedate[]='';
			}

			$StatisticDataCat=array_unique($StatisticDataCat);		
			foreach ($seriesData as $value) 
			{

				foreach($StatisticDataCat as $testData)
				{				
					$count=0;
					foreach($d as $sqdata)
					{	
						foreach($sqdata as $stemp)
						{
							if($stemp->answer == $value &&  $stemp->surveyName == $testData)
							{
								$count=$count+1;
							}
						}
					}

					$temp[]=['qname'=>$value,'sname'=>$testData,'count'=>$count];

				}								


			}	
			foreach($StatisticDataCat as $value)
			{
				$tot=0;
				foreach($temp as $kip)
				{
					if($kip['sname'] == $value)
					{
						$tot=$tot+(int)$kip['count'];
					}
				}
				$surveyanalytic[]=['sname'=>$value,'total'=>$tot];
			}
			foreach ($seriesData as $value) 
			{
				$dataStat=array();
				foreach($StatisticDataCat as $testData)
				{				
					$count=0;
					foreach($d as $sqdata)
					{	
						foreach($sqdata as $stemp)
						{
							if($stemp->answer == $value &&  $stemp->surveyName == $testData)
							{
								$count=$count+1;
							}
						}
					}
					foreach($surveyanalytic as $com)
					{
						if($com['sname'] == $testData && (int)$com['total'] !=0)
						{
							$percent=(float)($count/(int)$com['total'])*100;
							array_push($dataStat,$percent);
						}

					}
				}								
				$statCollection[]=['name'=>$value,'data'=>$dataStat];

			}	
			foreach($d as $qdata)
			{	
				foreach($qdata as $temp)
				{
					if(in_array($temp->question,$questions))
						continue;
					else
					{
						array_push($questions, $temp->question);					
					}
				}

			}
			$quesNo=0;
			foreach($questions as $ques)
			{	
				$quesNo=$quesNo+1;
				$tvalues=array();			
				foreach($d as $qdata)
				{
					foreach($qdata as $temp)
					{
						if($ques==$temp->question)	
						{				
							array_push($tvalues,$temp->answer);
						}

					}	
				}	
				$data1[]=['name'=>$ques,'data'=>$tvalues];
				$category[]=['name'=>$ques,'tooltip'=>'Q'.$quesNo,'categories'=>array_values(array_unique($tvalues))];

			}	
			$seriousValues=array();	
			foreach ($data1 as $val) {			
				$vals = array_count_values($val['data']);
				$totalCount=array_sum($vals);
			//echo 'No. of NON Duplicate Items: '.count($vals).'<br><br>';
				foreach($vals as $t)
				{
					array_push($seriousValues,(($t/$totalCount)*100));	
				}	
			//print_r($seriousValues);

			}		
			$returnData[]=['category'=>$category,'series'=>$seriousValues,'staticsCategory'=>array_values($StatisticDataCat),'staticsSeries'=>$statCollection,'pieDataCollection'=>$piedate,'questionData'=>$surevyQuestionsYN,'commentQuestion'=>$customquestions,'customDataCollection'=>$customGridJson,'allQuestionData'=>$surevyQuestionsAllStat,'pieAllStatData'=>$pieAllStatdata,'totalCount'=>$totalSuveyCount];
			echo json_encode($returnData);	
			exit();
		}     
		catch( PDOException $e ){
			print_r( $e );
		}

	}
}
if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$User_Surevy_Str = "SELECT id,survey,start_date,end_date,SurveyName,surveyDep from questions_survey WHERE status='1'";
	$User_Surevy_Sql=$db->prepare($User_Surevy_Str);
	$User_Surevy_Sql->execute(); 
	$userSurveyData= $User_Surevy_Sql->fetchAll();
	//$userSurveyData['start_date']=date("d-m-Y", strtotime($userSurveyData['start_date']));
	echo json_encode($userSurveyData);

}


?>	

