
<!DOCTYPE html>
<?php
error_reporting(E_ALL ^ E_DEPRECATED);?>

<html lang="en">
<link rel="stylesheet" type="text/css" media="screen" href="http://cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.css" />
<style type="text/css">
  a.fancybox img {
    border: none;
    box-shadow: 0 1px 7px rgba(0,0,0,0.6);
    -o-transform: scale(1,1); -ms-transform: scale(1,1); -moz-transform: scale(1,1); -webkit-transform: scale(1,1); transform: scale(1,1); -o-transition: all 0.2s ease-in-out; -ms-transition: all 0.2s ease-in-out; -moz-transition: all 0.2s ease-in-out; -webkit-transition: all 0.2s ease-in-out; transition: all 0.2s ease-in-out;
  } 
  a.fancybox:hover img {
    position: relative; z-index: 999; -o-transform: scale(1.03,1.03); -ms-transform: scale(1.03,1.03); -moz-transform: scale(1.03,1.03); -webkit-transform: scale(1.03,1.03); transform: scale(1.03,1.03);
  }
</style>

<style>


  iframe {width:100%}
</style>
<script>
 $(document).ready(function () {
  function hiding(){
   $( '#div1').hide();
 }
});
</script>
<script>
 $(document).ready(function () {
  function hiding(){
   $( '#div1').hide();
 }
});
</script>
<script>

 function switchVisible() {
  if (document.getElementById('Div1')) {

    if (document.getElementById('Div1').style.display == 'none') {
      document.getElementById('Div1').style.display = 'block';
      document.getElementById('Div2').style.display = 'none';
    }
    else {
      document.getElementById('Div1').style.display = 'none';
      document.getElementById('Div2').style.display = 'block';
    }
  }
}

</script>



<head>
  <meta charset="utf-8">
  <title>EMPLOYEE PORTAL| DASHBOARD</title>
  <script src="js/html5-trunk.js"></script>
  <link href="icomoon/style.css" rel="stylesheet">
  <link href="css/main.css" rel="stylesheet">
  <link href="css/fullcalendar.css" rel="stylesheet">
  <link href="css/nvd-charts.css" rel="stylesheet">
  
</head>
<body>
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/1.3.4/jquery.fancybox-1.3.4.pack.min.js"></script>
  <script type="text/javascript">
    $(function($){
      var addToAll = false;
      var gallery = true;
      var titlePosition = 'inside';
      $(addToAll ? 'img' : 'img.fancybox').each(function(){
        var $this = $(this);
        var title = $this.attr('title');
        var src = $this.attr('data-big') || $this.attr('src');
        var a = $('<a href="#" class="fancybox"></a>').attr('href', src).attr('title', title);
        $this.wrap(a);
      });
      if (gallery)
        $('a.fancybox').attr('rel', 'fancyboxgallery');
      $('a.fancybox').fancybox({
        titlePosition: titlePosition
      });
    });
    $.noConflict();
  </script>
  <!-- Header -->

  
  <?php include('./includes/header.php')?>

  <div class="container-fluid">
    <!-- Navigation ----------------------------------------------------------------Navigation-----------------------Navigation --------->
    <div id="mainnav" class="hidden-phone hidden-tablet">
      <ul>
        <li class="active">
          <span class="current-arrow"></span>
          <a href="index1.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe0a1;"></span>
            </div>
            Dashboard
          </a>
        </li>
        <li>
          <a href="my profile.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe071;"></span>
            </div>
            My Profile
          </a>
        </li>
        <li>
          <a href="Whatsup.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon="&#xe1b3;"></span>
            </div>
            WHATSAPP
          </a>
        </li>
        <li>
          <a href="staffpolling.php">
            <div class="icon">
              <span class="fs1" aria-hidden="true" data-icon=""></span>
            </div>
            STAFF POLLING
          </a>
        </li>
      </ul>
    </div>
    <div class="dashboard-wrapper">
      <div class="main-container">
        <!----------------------------------------------------navigationMini------------------------------------------------------------------------- -->
        <div class="navbar hidden-desktop">
          <div class="navbar-inner">
            <div class="container">
              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </a>
              <div class="nav-collapse collapse navbar-responsive-collapse">
                <ul class="nav">
                  <li>
                    <a href="index1.php">Dashboard</a>
                  </li>
                  <li>
                    <a href="my profile.php">My Profile</a>
                  </li>
                  <li>
                    <a href="whatsup.php">What's Up</a>
                  </li>
                  <li>
                    <a href="staffpolling.php">Staff Polling</a>
                  </li>
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row-fluid">
          <div class="span12">
            <ul class="breadcrumb-beauty">
              <li>
                <!-------------------- EDIT HERE DASHBOARD ---------------------->
                <a href="index.html"><span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span> Dashboard</a> 
                
              </li>
              <li>

              </li>
            </ul>
          </div>
        </div>

        <br>

        
        <div class="row-fluid">

          <div class="span4">

            <div class="widget no-margin">
              <div class="widget-header">
                <div class="title">
                  <span class="fs1" aria-hidden="true" data-icon=""></span> LATEST NEWS&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;   
                </div>
                <div class="tools pull-right">
                </div>				  
              </div>
              <div class="widget-body">
                <div id="content">



                </div>

                <!--content-->
                <div id='calendar'></div>
                
              </div>
            </div>
          </div>

          
          
          


          <div id="div1">	
            <div class="span8">

              <div class="widget no-margin">
                <div class="widget-header">
                  <div class="icontype">

                  </div>
                  
                </div>
                <div class="widget-body">
                  <script src="promotions/js/jquery-1.11.3.min.js" type="text/javascript"></script>
                  <script src="promotions/js/jssor.slider-22.1.8.mini.js" type="text/javascript"></script>
                  <script type="text/javascript">
                    jQuery(document).ready(function ($) {

                      var jssor_1_SlideshowTransitions = [
                      {$Duration:1200,$Zoom:11,$Rotate:-1,$Easing:{$Zoom:$Jease$.$InQuad,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2,$Round:{$Rotate:0.5},$Brother:{$Duration:1200,$Zoom:1,$Rotate:1,$Easing:$Jease$.$Swing,$Opacity:2,$Round:{$Rotate:0.5},$Shift:90}},
                      {$Duration:1400,x:0.25,$Zoom:1.5,$Easing:{$Left:$Jease$.$InWave,$Zoom:$Jease$.$InSine},$Opacity:2,$ZIndex:-10,$Brother:{$Duration:1400,x:-0.25,$Zoom:1.5,$Easing:{$Left:$Jease$.$InWave,$Zoom:$Jease$.$InSine},$Opacity:2,$ZIndex:-10}},
                      {$Duration:1200,$Zoom:11,$Rotate:1,$Easing:{$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2,$Round:{$Rotate:1},$ZIndex:-10,$Brother:{$Duration:1200,$Zoom:11,$Rotate:-1,$Easing:{$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2,$Round:{$Rotate:1},$ZIndex:-10,$Shift:600}},
                      {$Duration:1500,x:0.5,$Cols:2,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InOutCubic},$Opacity:2,$Brother:{$Duration:1500,$Opacity:2}},
                      {$Duration:1500,x:-0.3,y:0.5,$Zoom:1,$Rotate:0.1,$During:{$Left:[0.6,0.4],$Top:[0.6,0.4],$Rotate:[0.6,0.4],$Zoom:[0.6,0.4]},$Easing:{$Left:$Jease$.$InQuad,$Top:$Jease$.$InQuad,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2,$Brother:{$Duration:1000,$Zoom:11,$Rotate:-0.5,$Easing:{$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2,$Shift:200}},
                      {$Duration:1500,$Zoom:11,$Rotate:0.5,$During:{$Left:[0.4,0.6],$Top:[0.4,0.6],$Rotate:[0.4,0.6],$Zoom:[0.4,0.6]},$Easing:{$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2,$Brother:{$Duration:1000,$Zoom:1,$Rotate:-0.5,$Easing:{$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2,$Shift:200}},
                      {$Duration:1500,x:0.3,$During:{$Left:[0.6,0.4]},$Easing:{$Left:$Jease$.$InQuad,$Opacity:$Jease$.$Linear},$Opacity:2,$Outside:true,$Brother:{$Duration:1000,x:-0.3,$Easing:{$Left:$Jease$.$InQuad,$Opacity:$Jease$.$Linear},$Opacity:2}},
                      {$Duration:1200,x:0.25,y:0.5,$Rotate:-0.1,$Easing:{$Left:$Jease$.$InQuad,$Top:$Jease$.$InQuad,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2,$Brother:{$Duration:1200,x:-0.1,y:-0.7,$Rotate:0.1,$Easing:{$Left:$Jease$.$InQuad,$Top:$Jease$.$InQuad,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2}},
                      {$Duration:1600,x:1,$Rows:2,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2,$Brother:{$Duration:1600,x:-1,$Rows:2,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2}},
                      {$Duration:1600,x:1,$Rows:2,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2,$Brother:{$Duration:1600,x:-1,$Rows:2,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2}},
                      {$Duration:1600,y:-1,$Cols:2,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2,$Brother:{$Duration:1600,y:1,$Cols:2,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2}},
                      {$Duration:1200,y:1,$Easing:{$Top:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2,$Brother:{$Duration:1200,y:-1,$Easing:{$Top:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2}},
                      {$Duration:1200,x:1,$Easing:{$Left:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2,$Brother:{$Duration:1200,x:-1,$Easing:{$Left:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2}},
                      {$Duration:1200,y:-1,$Easing:{$Top:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2,$ZIndex:-10,$Brother:{$Duration:1200,y:-1,$Easing:{$Top:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2,$ZIndex:-10,$Shift:-100}},
                      {$Duration:1200,x:1,$Delay:40,$Cols:6,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:{$Left:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2,$ZIndex:-10,$Brother:{$Duration:1200,x:1,$Delay:40,$Cols:6,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:{$Top:$Jease$.$InOutQuart,$Opacity:$Jease$.$Linear},$Opacity:2,$ZIndex:-10,$Shift:-100}},
                      {$Duration:1500,x:-0.1,y:-0.7,$Rotate:0.1,$During:{$Left:[0.6,0.4],$Top:[0.6,0.4],$Rotate:[0.6,0.4]},$Easing:{$Left:$Jease$.$InQuad,$Top:$Jease$.$InQuad,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2,$Brother:{$Duration:1000,x:0.2,y:0.5,$Rotate:-0.1,$Easing:{$Left:$Jease$.$InQuad,$Top:$Jease$.$InQuad,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuad},$Opacity:2}},
                      {$Duration:1600,x:-0.2,$Delay:40,$Cols:12,$During:{$Left:[0.4,0.6]},$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Assembly:260,$Easing:{$Left:$Jease$.$InOutExpo,$Opacity:$Jease$.$InOutQuad},$Opacity:2,$Outside:true,$Round:{$Top:0.5},$Brother:{$Duration:1000,x:0.2,$Delay:40,$Cols:12,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Assembly:1028,$Easing:{$Left:$Jease$.$InOutExpo,$Opacity:$Jease$.$InOutQuad},$Opacity:2,$Round:{$Top:0.5}}}
                      ];

                      var jssor_1_options = {
                        $AutoPlay: true,
                        $FillMode: 5,
                        $SlideshowOptions: {
                          $Class: $JssorSlideshowRunner$,
                          $Transitions: jssor_1_SlideshowTransitions,
                          $TransitionsOrder: 1
                        },
                        $BulletNavigatorOptions: {
                          $Class: $JssorBulletNavigator$
                        }
                      };

                      var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                      /*responsive code begin*/
                      /*you can remove responsive code if you don't want the slider scales while window resizing*/
                      function ScaleSlider() {
                        var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                        if (refSize) {
                          refSize = Math.min(refSize, 600);
                          jssor_1_slider.$ScaleWidth(refSize);
                        }
                        else {
                          window.setTimeout(ScaleSlider, 30);
                        }
                      }
                      ScaleSlider();
                      $(window).bind("load", ScaleSlider);
                      $(window).bind("resize", ScaleSlider);
                      $(window).bind("orientationchange", ScaleSlider);
                      /*responsive code end*/
                    });
</script>

</div>
</div>
</div>

</div>
</div><!-- dashboard-container -->
</div>
</div>
<!-- container-fluid -->
<?php include('./includes/footer.php')?>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery-ui-1.8.23.custom.min.js"></script>

<!-- morris charts -->
<script src="js/morris/morris.js"></script>
<script src="js/morris/raphael-min.js"></script>

<!-- Flot charts -->
<script src="js/flot/jquery.flot.js"></script>
<script src="js/flot/jquery.flot.resize.min.js"></script>

<!-- Calendar Js -->
<script src="js/fullcalendar.js"></script>

<!-- Tiny Scrollbar JS -->
<script src="js/tiny-scrollbar.js"></script>

<!-- custom Js -->
<script src="js/custom-index.js"></script>
<!--<script src="js/custom.js"></script>-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
  $(window).load(function() {
    $(".success-box").delay(5000).slideUp(500);
  });
</script> 



<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>

<!-- Flot charts -->
<script src="js/flot/jquery.flot.js"></script>
<script src="js/flot/jquery.flot.selection.js"></script>
<script src="js/flot/jquery.flot.resize.min.js"></script>

<!-- NVD3 charts -->
<script src="js/nvd/d3.v2.js"></script>
<script src="js/nvd/nv.d3.js"></script>
<script src="js/nvd/tooltip.js"></script>
<script src="js/nvd/utils.js"></script>
<script src="js/nvd/legend.js"></script>
<script src="js/nvd/axis.js"></script>
<script src="js/nvd/scatter.js"></script>
<script src="js/nvd/line.js"></script>
<script src="js/nvd/lineWithFocusChart.js"></script>
<script src="js/nvd/stream_layers.js"></script>

<!-- Google Visualization JS -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<!-- customjs -->
<script src="js/custom-graphs.js"></script>
</body>
</html>
